# Le site web Vikazimut

## Le projet Vikazimut

Vikazimut est un projet d'étudiants en informatique de l'[ENSICAEN](https://www.ensicaen.fr).

Le projet répond à une demande du club normand de course d'orientation et de raids multisports [Vik'Azim](https://vikazim.fr)
visant la réalisation d'une application mobile pour la pratique de la course d'orientation
en autonomie.

L’application mobile Vikazimut remplace la carte papier, la boussole et le poinçon de validation des postes de contrôle.
Un parcours d'orientation consiste en une suite de postes de contrôle matérialisés sur le terrain
par une balise type fédération internationale de course d’orientation (IOF).
L’orienteur utilise l’application pour se repérer à partir de la carte et
valider son passage aux points de contrôle en utilisant le lecteur de code QR,
le lecteur de tag NFC ou le lecteur de iBeacons selon l'équipement qui est associé au poste de contrôle.
Un mode automatique permet la validation directement à partir de la
position GPS sans utiliser d'équipement physique au poste de contrôle.

Le site web compagnon fournit les parcours d'orientation sous la forme
d'une carte avec les positions des postes de contrôle.
Ces parcours sont proposés par des clubs, des enseignants d'EPS, des collectivités ou des
particuliers qui ont un compte sur le site.
Le site web offre aussi des outils d'analyse rétrospective de la performance réalisée
pour les parcours dont les orienteurs ont volontairement téléversé la trace sur le site.

## Le site web

La présente forge correspond au site web (frontend) de l'application [Vikazimut](https://vikazimut.vikazim.fr).
Le site Web propose trois fonctionnalités aux orienteurs :

- Une liste de parcours d'orientation créés par des traceurs référencés sur le serveur,  

- La liste des traces des courses réalisées par des orienteurs qui ont volontairement choisi
de téléverser leur trace sur le serveur.
Le site fournit des outils d'analyse comparative ou individuelle de la performance à partir de ces traces.
 
- La liste des événements créés par des traceurs, auxquels les orienteurs peuvent s'inscrire.
Un événement combine généralement plusieurs courses et se déroule sur un ou plusieurs jours.
Les orienteurs inscrits reportent leurs traces sur les différents parcours
sur le serveur sur la base desquelles le serveur construit un classement.

## Code source

Le projet est développé avec le kit de développement Flutter et le langage Dart.
Le code source du site web Vikazimut est accessible sur le GitLab du projet&nbsp;:
[https://gitlab.com/clouardregis/vikazimut-website](https://gitlab.com/clouardregis/vikazimut-website).

## Documentation technique

L'ensemble des documentations techniques des outils d'analyse de performance
proposés sur le site Web est accessible à l'adresse&nbsp;:

[https://gitlab.com/clouardregis/vikazimut-website/-/wikis/Home](https://gitlab.com/clouardregis/vikazimut-website/-/wikis/Home)(https://gitlab.com/clouardregis/vikazimut-website/-/wikis/Home)

## Licence

Le projet est à code source ouvert, libre et gratuit.
Il est placé sous la licence publique générale GNU amoindrie [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).

    Licence LGPL V3

    Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conformément
    aux dispositions de la Licence Publique Générale GNU, telle que publiée par la Free Software
    Foundation ; version 2 de la licence, ou encore (à votre choix) toute version ultérieure.

    Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même
    la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN USAGE PARTICULIER.
    Pour plus de détail, voir la Licence Publique Générale GNU.

## Documentation scientifique des outils d'analyse de la performance

- Lien vers la [documentation relative au code des outils d'analyse de la performance](https://gitlab.com/clouardregis/vikazimut-website/-/wikis/Home).

## Projets liés

- L'application mobile développée en Flutter - Android et iOS ([dépôt gitlab](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app)).
- Le serveur web développé en Symfony - partie backend du siste web ([dépôt gitlab](https://gitlab.com/clouardregis/vikazimut-server)).

## Contributeurs ([ENSICAEN](https://www.ensicaen.fr/) - [spécialité informatique](https://www.ensicaen.fr/formations/ingenieur-informatique/)
et [Université de Caen Normandie](https://www.unicaen.fr) - [spécialité informatique](https://www.info.unicaen.fr/licence/info))

### 2019/2020 Université de Caen

- Martin FÉAUX DE LA CROIX

### 2020/2021 Université de Caen

- Antoine BOITEAU
- Suliac LAVENANT

### 2021/2022 ENSICAEN

- Ilyass AZIRAR
- Amine KHALFAOUI
- Ilias LAHBABI
- Reda RGUIG

### 2021/2022 Université de Caen

- Quentin LIREUX

### 2022/2023 Université de Caen

- Mohamed AKADDAR

### 2023/2024 ENSICAEN

- Blaise HECQUET 
- Moris Lorys KAMGANG 
- Tom LE CAM
- Come LESCARMONTIER
- Robin SALMI

### Traduction

* Português : Margarida GONÇALVES NOVO
