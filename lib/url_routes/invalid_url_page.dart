// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';

@immutable
class InvalidUrlPage extends StatelessWidget {
  const InvalidUrlPage();

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '404',
              style: Theme.of(context).textTheme.displayLarge!,
            ),
            const SizedBox(height: 25),
            Text(
              L10n.getString("error_page_title").toUpperCase(),
              style: Theme.of(context).textTheme.displaySmall!.copyWith(color: kOrangeColor),
            ),
            const SizedBox(height: 15),
            Text(L10n.getString("invalid_url_error")),
          ],
        ),
      ),
    );
  }
}
