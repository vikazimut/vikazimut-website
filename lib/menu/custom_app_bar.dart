// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/utils/utils.dart';

/// Beautiful App Bar Background with patterns
class CustomAppBarBackground extends StatelessWidget {
  final Color color;

  const CustomAppBarBackground(this.color);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      foregroundPainter: _CustomAppBarPainter(color),
    );
  }
}

class _CustomAppBarPainter extends CustomPainter {
  final Color color;

  const _CustomAppBarPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    Paint smallRectanglePaint = Paint()
      ..color = color.withValues(alpha: 0.15)
      ..style = PaintingStyle.fill;

    Paint bigRectanglePaint = Paint()
      ..color = color.withValues(alpha: 0.2)
      ..style = PaintingStyle.fill;

    canvas.clipRect(Rect.fromLTWH(0, 0, size.width, size.height));
    // Left small rectangle
    canvas.save();
    canvas.translate(-90, 0);
    canvas.skew(deg2rad(25), deg2rad(-12));
    canvas.drawRRect(RRect.fromRectXY(const Rect.fromLTWH(0, 0, 300, 150), 6, 6), smallRectanglePaint);
    canvas.restore();

    // Left big rectangle
    canvas.save();
    canvas.translate(0, 20);
    canvas.skew(deg2rad(-25), deg2rad(10));
    canvas.drawRRect(RRect.fromRectXY(const Rect.fromLTWH(0, 0, 150, 600), 4, 4), bigRectanglePaint);
    canvas.restore();

    // Right big rectangle
    canvas.save();
    canvas.translate(size.width - 220, 0);
    canvas.skew(deg2rad(25), deg2rad(-10));
    canvas.drawRRect(RRect.fromRectXY(const Rect.fromLTWH(0, 0, 300, 150), 4, 4), smallRectanglePaint);
    canvas.restore();

    // Right small rectangle
    canvas.save();
    canvas.translate(size.width - 110, 25);
    canvas.skew(deg2rad(25), deg2rad(10));
    canvas.drawRRect(RRect.fromRectXY(const Rect.fromLTWH(0, 0, 150, 600), 6, 6), bigRectanglePaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
