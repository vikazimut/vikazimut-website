import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;
import 'package:vikazimut_website/admin_pages/admin_download_stats_page.dart';
import 'package:vikazimut_website/admin_pages/admin_event_list_page.dart';
import 'package:vikazimut_website/admin_pages/admin_mailing_page.dart';
import 'package:vikazimut_website/admin_pages/admin_planner_list/admin_planner_list_view.dart';
import 'package:vikazimut_website/authentication/authentication_helper.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/planner_pages/my_partners//list/community_list_page_view.dart';
import 'package:vikazimut_website/planner_pages/home/planner_home_view.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_list_page.dart';
import 'package:vikazimut_website/planner_pages/my_events/planner_event_list_view.dart';
import 'package:vikazimut_website/planner_pages/planner_download_stats_view.dart';
import 'package:vikazimut_website/planner_pages/profile/planner_profile_page.dart';
import 'package:vikazimut_website/public_pages/courses/worldmap/worldmap_page_view.dart';
import 'package:vikazimut_website/public_pages/events/event_list_page_view.dart';
import 'package:vikazimut_website/public_pages/home/home_page_view.dart';
import 'package:vikazimut_website/public_pages/login/login_page_view.dart';

import 'header_item.dart';

final List<HeaderItem> publicActionItems = [
  HeaderItem(
    title: "menu_home",
    onTap: (context) => GoRouter.of(context).go(HomePageView.routePath),
    icon: Icons.home,
  ),
  HeaderItem(
    title: "menu_courses",
    onTap: (context) => GoRouter.of(context).go(WorldMapPageView.routePath),
    icon: Icons.map,
  ),
  HeaderItem(
    title: "menu_events",
    onTap: (context) => GoRouter.of(context).go(EventListPageView.routePath),
    icon: Icons.event,
  ),
  HeaderItem(
    title: "menu_us",
    onTap: (context) => GoRouter.of(context).go(HomePageView.aboutUsRoutePath),
    icon: Icons.person,
  ),
];

final List<HeaderItem> helpActionItems = [
  HeaderItem(
    title: "menu_contact",
    onTap: (context) => url_launcher.launchUrl(Uri.parse("https://www.facebook.com/groups/1319128481622517")),
    icon: Icons.facebook_outlined,
  ),
  HeaderItem(
    title: "menu_faq",
    onTap: (context) => url_launcher.launchUrl(Uri.parse("${constants.SERVER}/public/faq.html")),
    icon: Icons.help_outline,
  ),
  HeaderItem(
    title: "menu_tutorial",
    onTap: (context) => url_launcher.launchUrl(Uri.parse("${constants.SERVER}/public/tutorials.html")),
    icon: Icons.help_outline,
  ),
];

final List<HeaderItem> plannerActionItems = [
  HeaderItem(
    title: "menu_planner_home",
    onTap: (context) => GoRouter.of(context).go(PlannerHomeView.routePath),
    icon: Icons.account_balance_outlined,
  ),
  HeaderItem(
    title: "menu_planner_courses",
    onTap: (context) => GoRouter.of(context).go("${PlannerCourseListPage.routePath}/-1"),
    icon: Icons.location_on,
  ),
  HeaderItem(
    title: "menu_planner_events",
    onTap: (context) => GoRouter.of(context).go("${PlannerEventListView.routePath}/-1"),
    icon: Icons.event,
  ),
  HeaderItem(
    title: "menu_planner_community",
    onTap: (context) => GoRouter.of(context).go("${CommunityListPageView.routePath}/-1"),
    icon: Icons.event,
  ),
  HeaderItem(
    title: "menu_planner_statistics",
    onTap: (context) => GoRouter.of(context).go(PlannerDownloadStatsPage.routePath),
    icon: Icons.list_alt_outlined,
  ),
  HeaderItem(
    title: "menu_personal_information",
    onTap: (context) => GoRouter.of(context).go("${PlannerProfilePage.routePath}/-1"),
    icon: Icons.perm_contact_cal_outlined,
  ),
];

final List<HeaderItem> loginActionItems = [
  HeaderItem.empty(),
  HeaderItem(
    title: "menu_login",
    onTap: (context) => GoRouter.of(context).go(LoginPageView.routePath),
    icon: Icons.person_outline,
  ),
];

final List<HeaderItem> logoutActionItems = [
  HeaderItem.empty(),
  HeaderItem(
    title: "menu_logout",
    onTap: (context) {
      AuthenticationHelper.logout();
      GoRouter.of(context).go(HomePageView.routePath);
    },
    icon: Icons.login_outlined,
  ),
];

final List<HeaderItem> adminActionItems = [
  HeaderItem(
    title: "menu_admin_planner_list",
    onTap: (context) => GoRouter.of(context).go(AdminPlannerListView.routePath),
    icon: Icons.people,
  ),
  HeaderItem(
    title: "menu_admin_course_list",
    onTap: (context) => GoRouter.of(context).go(AdminDownloadStatsPage.routePath),
    icon: Icons.location_on,
  ),
  HeaderItem(
    title: "menu_admin_event_list",
    onTap: (context) => GoRouter.of(context).go(AdminEventListPage.routePath),
    icon: Icons.event,
  ),
  HeaderItem(
    title: "menu_admin_mailing",
    onTap: (context) => GoRouter.of(context).go(AdminMailingPage.routePath),
    icon: Icons.mail_outline,
  ),
];
