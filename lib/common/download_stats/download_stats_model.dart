import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/discipline.dart';

class DownloadStatsModel {
  static const int MONTH_IN_MS = 2629746000;
  final int id;
  final String name;
  final int createdAtInMilliseconds;
  final int downloadCount;
  late double downloadPerMonth;
  final int traceCount;
  final bool touristic;
  final bool closed;
  final bool locked;
  final Discipline? discipline;

  DownloadStatsModel({
    required this.id,
    required this.name,
    required this.createdAtInMilliseconds,
    required this.downloadCount,
    required this.traceCount,
    required this.touristic,
    required this.closed,
    required this.locked,
    required this.discipline,
  }) {
    downloadPerMonth = computeDownloadPerMonth_(downloadCount, DateTime.now().millisecondsSinceEpoch - createdAtInMilliseconds);
  }

  factory DownloadStatsModel.fromJson(json) {
    return DownloadStatsModel(
      id: json["id"] as int,
      name: json["name"] as String,
      createdAtInMilliseconds: json["createdAtInMilliseconds"] as int,
      downloadCount: json["downloadCount"] as int,
      traceCount: json["traceCount"] as int,
      touristic: json["touristic"] as bool,
      closed: json["closed"] as bool,
      locked: json["locked"] as bool,
      discipline: Discipline.toEnum(json["discipline"] as int?),
    );
  }

  @visibleForTesting
  static double computeDownloadPerMonth_(int downloadCount, int delay) {
    final int laps = delay ~/ MONTH_IN_MS + 1;
    return downloadCount / laps;
  }
}
