// coverage:ignore-file
library;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_course_page/planner_event_course_view.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_detail_page/planner_event_detail_page.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_participants/planner_event_participant_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'event_list_model.dart';
import 'event_list_presenter.dart';

part 'event_add_form.dart';

class EventListPage extends StatefulWidget {
  final String title;
  final bool isAdmin;
  final String url;
  late final int plannerId;

  EventListPage({required this.title, required this.isAdmin, required this.url, String? args}) {
    if (args == null) {
      plannerId = -1;
    } else {
      var x = int.tryParse(args);
      if (x == null) {
        plannerId = -1;
      } else {
        plannerId = x;
      }
    }
  }

  @override
  State<EventListPage> createState() => EventListPageState();
}

class EventListPageState extends State<EventListPage> {
  final ScrollController _scrollController = ScrollController();
  late final EventListPresenter _presenter;
  final ValueNotifier<int> _eventCount = ValueNotifier<int>(-1);
  Future<List<EventData>>? _events;
  String? _errorMessage;

  @override
  void initState() {
    _presenter = EventListPresenter(this);
    _events = _presenter.fetchEvents(widget.url, widget.plannerId);
    _events?.then<void>((List<EventData> list) => _eventCount.value = list.length).catchError((_) {});
    super.initState();
  }

  @override
  void dispose() {
    _eventCount.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: ValueListenableBuilder<int>(
                      valueListenable: _eventCount,
                      builder: (context, value, child) {
                        return Text(
                          L10n.getString(widget.title) + (value < 0 ? "" : " ($value)"),
                          style: Theme.of(context).textTheme.displayMedium,
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 10),
                  FutureBuilder<List<EventData>>(
                    future: _events,
                    builder: (context, AsyncSnapshot<List<EventData>> snapshot) {
                      if (snapshot.hasError) {
                        return Expanded(child: GlobalErrorWidget(snapshot.error.toString()));
                      } else if (!snapshot.hasData) {
                        return const Expanded(child: Center(child: CircularProgressIndicator()));
                      } else {
                        return Expanded(
                          child: Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(width: 1, color: kCaptionColor),
                              color: Theme.of(context).scaffoldBackgroundColor,
                            ),
                            child: Scrollbar(
                              controller: _scrollController,
                              thumbVisibility: true,
                              child: ListView.builder(
                                key: PageStorageKey<String>(widget.isAdmin ? "planner_event_list" : "admin_event_list"),
                                controller: _scrollController,
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, i) {
                                  final index = snapshot.data!.length - 1 - i;
                                  return _buildCourseControlPanel(context, snapshot.data![index].id, snapshot.data![index].name);
                                },
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          if (_errorMessage != null) ErrorContainer(_errorMessage!),
          if (!ScreenHelper.isMobile(context)) const SizedBox(height: 8),
          if (widget.plannerId <= 0 && !widget.isAdmin)
            CustomPlainButton(
              label: L10n.getString("planner_event_list_page_button_add_event"),
              backgroundColor: kOrangeColor,
              onPressed: () async {
                EventFormData? data = await AddCourseFormDialog().execute(context);
                if (data != null) {
                  _presenter.addEvent(data);
                }
              },
            ),
          Footer(),
        ],
      ),
    );
  }

  Widget _buildCourseControlPanel(BuildContext context, int id, String title) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Text(
            title,
            style: Theme.of(context).textTheme.displaySmall,
          ),
        ),
        Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.center,
          spacing: 5,
          runSpacing: 5,
          children: [
            CustomPlainButton(
              label: L10n.getString("planner_event_list_page_button_detail"),
              backgroundColor: kSuccessColor,
              onPressed: () => GoRouter.of(context).go("${PlannerEventDetailPage.routePath}/$id"),
            ),
            CustomPlainButton(
              label: L10n.getString("planner_event_list_page_button_courses"),
              backgroundColor: kInfoColor,
              onPressed: () => GoRouter.of(context).go("${PlannerEventCourseView.routePath}/$id"),
            ),
            CustomPlainButton(
              label: L10n.getString("planner_event_list_page_button_participants"),
              backgroundColor: kWarningColor,
              foregroundColor: Colors.black,
              onPressed: () => GoRouter.of(context).go("${PlannerEventParticipantView.routePath}/$id"),
            ),
            CustomPlainButton(
              label: L10n.getString("planner_event_list_page_button_delete"),
              backgroundColor: kDangerColor,
              onPressed: () => showBinaryQuestionAlertDialog(
                context: context,
                title: L10n.getString("planner_event_list_page_delete_title"),
                message: L10n.getString("planner_event_list_page_delete_message"),
                okMessage: L10n.getString("planner_event_list_page_delete_ok_message"),
                noMessage: L10n.getString("planner_event_list_page_delete_no_message"),
                action: () {
                  _presenter.deleteEvent(id, title);
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _events = _presenter.fetchEvents(widget.url, widget.plannerId);
      _events?.then<void>((List<EventData> list) => _eventCount.value = list.length).catchError((_) {});
    });
  }

  void displayToast(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text(message)),
      );
  }

  void error(String message) {
    setState(() {
      _errorMessage = message;
    });
  }

  void redirectTo(String routePath) {
    GoRouter.of(context).replace(routePath);
  }
}
