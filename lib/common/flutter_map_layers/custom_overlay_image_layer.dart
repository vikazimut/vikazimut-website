// coverage:ignore-file
import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:vikazimut_website/utils/utils.dart';

@immutable
abstract class CustomBaseOverlayImage extends StatelessWidget {
  final ImageProvider? imageProvider;
  final double opacity;

  const CustomBaseOverlayImage({
    super.key,
    required this.imageProvider,
    this.opacity = 1,
  });
}

class CustomRotatedOverlayImage extends CustomBaseOverlayImage {
  final LatLngBounds bounds;
  final double angleInDegree;

  const CustomRotatedOverlayImage({
    required this.bounds,
    required super.imageProvider,
    super.opacity = 1.0,
    this.angleInDegree = 0,
  });

  @override
  Widget build(BuildContext context) {
    MapCamera map = MapCamera.of(context);
    final zoomScale = map.getZoomScale(map.zoom, map.zoom);
    final pixelOrigin = map.pixelOrigin;
    final upperLeftPixel = map.project(bounds.northWest) * zoomScale - pixelOrigin.toDoublePoint();
    final bottomRightPixel = map.project(bounds.southEast) * zoomScale - pixelOrigin.toDoublePoint();
    return Positioned(
      left: upperLeftPixel.x.toDouble(),
      top: upperLeftPixel.y.toDouble(),
      width: (bottomRightPixel.x - upperLeftPixel.x).toDouble(),
      height: (bottomRightPixel.y - upperLeftPixel.y).toDouble(),
      child: Transform.rotate(
        angle: deg2rad(angleInDegree),
        child: Image(
          image: imageProvider!,
          fit: BoxFit.fill,
          // Caveat: high is worst than medium when scaling down!
          filterQuality: FilterQuality.medium,
          color: Color.fromRGBO(255, 255, 255, opacity),
          colorBlendMode: BlendMode.modulate,
        ),
      ),
    );
  }
}

class CustomRotatedOverlayCanvas extends CustomBaseOverlayImage {
  final LatLngBounds bounds;
  final double angleInDegree;
  final Widget animationCanvas;

  const CustomRotatedOverlayCanvas({
    required this.bounds,
    super.opacity = 0.5,
    this.angleInDegree = 0,
    required this.animationCanvas,
  }) : super(imageProvider: null);

  @override
  Widget build(BuildContext context) {
    final MapCamera map = MapCamera.of(context);
    final zoomScale = map.getZoomScale(map.zoom, map.zoom);
    final Point<double> pixelOrigin = map.pixelOrigin;
    final upperLeftPixel = map.project(bounds.northWest) * zoomScale - pixelOrigin.toDoublePoint();
    final bottomRightPixel = map.project(bounds.southEast) * zoomScale - pixelOrigin.toDoublePoint();
    return Positioned(
      left: upperLeftPixel.x.toDouble(),
      top: upperLeftPixel.y.toDouble(),
      width: (bottomRightPixel.x - upperLeftPixel.x).toDouble(),
      height: (bottomRightPixel.y - upperLeftPixel.y).toDouble(),
      child: Transform.rotate(
        angle: deg2rad(angleInDegree),
        child: animationCanvas,
      ),
    );
  }
}

@immutable
class CustomOverlayImageLayer extends StatelessWidget {
  final List<CustomBaseOverlayImage> overlayImages;

  const CustomOverlayImageLayer({super.key, required this.overlayImages});

  @override
  Widget build(BuildContext context) => MobileLayerTransformer(
        child: ClipRect(
          child: Stack(children: overlayImages),
        ),
      );
}
