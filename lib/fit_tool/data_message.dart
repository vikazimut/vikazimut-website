import 'dart:typed_data';

import 'package:collection/collection.dart';

import 'definition_message.dart';
import 'developer_field.dart';
import 'field.dart';
import 'profile/messages/message_factory.dart';

abstract class DataMessage extends Message {
  DataMessage({
    this.name = '',
    super.globalId = 0,
    super.localId = 0,
    super.endian = Endian.little,
    this.definitionMessage,
    List<Field>? fields,
    List<DeveloperField>? developerFields,
  })  : fields = fields ?? [],
        developerFields = developerFields ?? [];

  final String name;
  final List<Field> fields;
  final List<DeveloperField> developerFields;
  DefinitionMessage? definitionMessage;

  static DataMessage fromDefinition(DefinitionMessage definitionMessage, List<DeveloperField> developerFields) {
    return MessageFactory.fromDefinition(definitionMessage, developerFields);
  }

  static DataMessage fromBytes(DefinitionMessage definitionMessage, List<DeveloperField> developerFields, Uint8List bytes) {
    final message = DataMessage.fromDefinition(definitionMessage, developerFields);
    message.readFromBytes(bytes);
    return message;
  }

  Field? getField(int id) {
    return fields.firstWhereOrNull((field) => field.id == id);
  }

  void _clearFieldById(int id) {
    final field = getField(id);
    if (field != null) {
      field.clear();
      if (definitionMessage != null) {
        definitionMessage!.removeField(id);
      }
    }
  }

  void clearDeveloperFieldById(int developerDataIndex, int id) {
    final field = getDeveloperField(developerDataIndex, id);
    if (field != null) {
      field.clear();
      if (definitionMessage != null) {
        definitionMessage!.removeDeveloperField(developerDataIndex, id);
      }
    }
  }

  @override
  void removeField(int id) {
    _clearFieldById(id);
  }

  @override
  void removeDeveloperField(int developerDataIndex, int id) {
    clearDeveloperFieldById(developerDataIndex, id);
  }

  DeveloperField? getDeveloperField(int developerDataIndex, int id) {
    return developerFields.firstWhereOrNull((field) => field.developerDataIndex == developerDataIndex && field.id == id);
  }

  void readFromBytes(Uint8List bytes) {
    var start = 0;

    if (definitionMessage == null) {
      throw Exception('DefinitionMessage cannot be null.');
    }

    for (var fieldDefinition in definitionMessage!.fieldDefinitions) {
      final field = getField(fieldDefinition.id);

      if (field == null) {
        start += fieldDefinition.size;
        continue;
      }

      if (field.isValid()) {
        final fieldBytes = Uint8List.sublistView(bytes, start, start + field.size);

        var subField = field.getValidSubField(fields);
        field.readAllFromBytes(fieldBytes, subField: subField, endian: endian);
        start += field.size;
      } else {
        throw Exception('Field ${field.name} is empty');
      }
    }

    for (var developerFieldDefinition in definitionMessage!.developerFieldDefinitions) {
      final field = getDeveloperField(developerFieldDefinition.developerDataIndex, developerFieldDefinition.id);

      if (field == null) {
        throw Exception('Developer Field id: ${developerFieldDefinition.developerDataIndex}:${developerFieldDefinition.id} is not defined for message $name:$globalId');
      }
      if (field.isValid()) {
        final fieldBytes = Uint8List.sublistView(bytes, start, start + field.size);
        var subField = field.getValidSubField(fields);
        field.readAllFromBytes(fieldBytes, subField: subField, endian: endian);
        start += field.size;
      } else {
        throw Exception('Developer Field ${field.name} is empty');
      }
    }
  }

  @override
  int get size {
    var count = 0;
    for (var field in fields) {
      if (field.isValid()) {
        count += field.size;
      }
    }
    for (var field in developerFields) {
      if (field.isValid()) {
        count += field.size;
      }
    }
    return count;
  }
}
