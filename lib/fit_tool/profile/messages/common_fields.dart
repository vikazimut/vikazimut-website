import '../../base_type.dart';
import '../../field.dart';

// ignore_for_file: constant_identifier_names

class TimestampField extends Field {
  TimestampField({
    super.size,
    super.growable = true,
  }) : super(
          name: 'timestamp',
          id: ID,
          type: BaseType.UINT32,
          offset: -631065600000,
          scale: 0.001,
          units: 'ms',
          mainTypeName: 'date_time',
          subFields: [],
        );

  static const ID = 253;
}

class MessageIndexField extends Field {
  MessageIndexField({
    super.size,
    super.growable = true,
  }) : super(
          name: 'message_index',
          id: ID,
          type: BaseType.UINT16,
          offset: 0,
          scale: 1,
          subFields: [],
        );

  static const ID = 254;
}
