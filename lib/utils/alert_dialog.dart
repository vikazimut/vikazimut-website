// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';

/// Credit: https://pub.dev/packages/commons

void showBinaryQuestionAlertDialog({
  required BuildContext context,
  required String title,
  required String message,
  required String okMessage,
  required String noMessage,
  required VoidCallback? action,
}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: kOrangeColor),
        ),
        content: Text(
          message,
          style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
              action?.call();
            },
            child: Text(
              okMessage,
              style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
            ),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              noMessage.toUpperCase(),
              style: const TextStyle(color: kOrangeColor),
            ),
          ),
        ],
      );
    },
  );
}

Future<void> showErrorDialog(BuildContext context, {required title, required String message}) async {
  const Color color = kRed;
  const Icon icon = Icon(Icons.close, size: 64, color: Colors.white);

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: L10n.getString("button_ok_label"),
    buttonColor: color,
  );
}

Future<void> showSuccessDialog(BuildContext context, {required String message}) async {
  const Color color = kSuccessColor;
  const Icon icon = Icon(Icons.check_box, size: 64, color: Colors.white);

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: "",
    color: color,
    message: message,
    positiveButtonText: L10n.getString("button_ok_label"),
    buttonColor: color,
  );
}

Future<void> showConfirmationDialog(
  BuildContext context, {
  required String title,
  required String message,
  required String yesButtonText,
  required String noButtonText,
  VoidCallback? yesButtonAction,
  VoidCallback? noButtonAction,
}) async {
  const color = kOrangeColor;
  const icon = Icon(Icons.help_outline, size: 64, color: Colors.white);

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: yesButtonText,
    positiveButtonAction: yesButtonAction,
    negativeButtonText: noButtonText,
    negativeButtonAction: noButtonAction,
    buttonColor: color,
  );
}

void showWarningDialog(
  BuildContext context, {
  required String title,
  required String message,
  required String yesButtonText,
  String? noButtonText,
  VoidCallback? yesAction,
  VoidCallback? noAction,
  String? checkboxMessage,
  VoidCallback? checkboxAction,
}) {
  const color = Color(0xffFF8C00);
  const icon = Icon(Icons.warning, size: 64, color: Colors.white);

  _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: yesButtonText,
    positiveButtonAction: yesAction,
    checkboxMessage: checkboxMessage,
    checkboxAction: checkboxAction,
    buttonColor: color,
  );
}

Future<void> _buildAndShowDialog({
  required BuildContext context,
  required Icon dialogIcon,
  required String title,
  required Color color,
  required String message,
  required String positiveButtonText,
  required Color buttonColor,
  String? negativeButtonText,
  VoidCallback? positiveButtonAction,
  VoidCallback? negativeButtonAction,
  String? checkboxMessage,
  VoidCallback? checkboxAction,
}) async {
  Widget dialogContent(
    BuildContext context, {
    required Icon dialogIcon,
    required String title,
    required Color color,
    required String message,
    required String positiveButtonText,
    String? negativeButtonText,
    VoidCallback? positiveButtonAction,
    VoidCallback? negativeButtonAction,
    String? checkboxMessage,
    VoidCallback? checkboxAction,
    required Color buttonColor,
  }) {
    var screenWidth = MediaQuery.of(context).size.width;
    List<bool> remember = [false];
    return Dialog(
      insetPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            width: screenWidth >= 600 ? 500 : screenWidth,
            padding: const EdgeInsets.only(
              top: 45.0 + 16.0,
              bottom: 16.0,
              left: 16.0,
              right: 16.0,
            ),
            margin: const EdgeInsets.only(top: 55.0),
            decoration: BoxDecoration(
              color: Theme.of(context).dialogTheme.backgroundColor,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(16.0),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (title.isNotEmpty)
                  Text(
                    title,
                    style: TextStyle(
                      color: color,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                const SizedBox(height: 16.0),
                Flexible(
                  fit: FlexFit.loose,
                  child: SingleChildScrollView(
                    child: Text(
                      message,
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                if (checkboxMessage != null && checkboxMessage.isNotEmpty)
                  CheckBoxContainer(
                    checkboxValue: remember,
                    checkboxMessage: checkboxMessage,
                    color: color,
                  ),
                ClipRect(
                  clipBehavior: Clip.hardEdge,
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        CustomPlainButton(
                            label: positiveButtonText,
                            autofocus: negativeButtonText == null,
                            backgroundColor: buttonColor,
                            onPressed: () {
                              if (remember[0] && checkboxAction != null) {
                                checkboxAction();
                              }
                              Navigator.of(context).pop();
                              if (positiveButtonAction != null) {
                                positiveButtonAction();
                              }
                            }),
                        const SizedBox(width: 5.0),
                        if (negativeButtonText != null)
                          CustomOutlinedButton(
                            autofocus: true,
                            label: negativeButtonText,
                            backgroundColor: buttonColor,
                            foregroundColor: buttonColor,
                            onPressed: () {
                              Navigator.of(context).pop();
                              if (negativeButtonAction != null) {
                                negativeButtonAction();
                              }
                            },
                          ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 16.0,
            right: 16.0,
            child: CircleAvatar(
              backgroundColor: color,
              radius: 45.0,
              child: dialogIcon,
            ),
          ),
        ],
      ),
    );
  }

  await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogContent(
          context,
          dialogIcon: dialogIcon,
          title: title,
          color: color,
          message: message,
          positiveButtonText: positiveButtonText,
          positiveButtonAction: positiveButtonAction,
          negativeButtonText: negativeButtonText,
          negativeButtonAction: negativeButtonAction,
          checkboxMessage: checkboxMessage,
          checkboxAction: checkboxAction,
          buttonColor: buttonColor,
        );
      });
}

class CheckBoxContainer extends StatefulWidget {
  final List<bool> checkboxValue;
  final String checkboxMessage;
  final Color color;

  const CheckBoxContainer({
    super.key,
    required this.checkboxValue,
    required this.color,
    required this.checkboxMessage,
  });

  @override
  CheckBoxContainerState createState() => CheckBoxContainerState();
}

class CheckBoxContainerState extends State<CheckBoxContainer> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Theme(
          data: ThemeData(primarySwatch: Colors.yellow, unselectedWidgetColor: widget.color),
          child: Checkbox(
            value: widget.checkboxValue[0],
            checkColor: Colors.white,
            activeColor: widget.color,
            onChanged: (value) {
              setState(() {
                widget.checkboxValue[0] = value!;
              });
            },
          ),
        ),
        Text(widget.checkboxMessage),
      ],
    );
  }
}

class QuestionDialog extends AlertDialog {
  QuestionDialog({required String title, required Widget content, required List<Widget> actions})
      : super(
          contentPadding: const EdgeInsets.fromLTRB(14.0, 20.0, 14.0, 20.0),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          title: Container(
            alignment: Alignment.center,
            width: double.infinity,
            decoration: const BoxDecoration(
              color: kGreenColor,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Text(
              title,
              style: const TextStyle(color: Colors.white),
            ),
          ),
          content: content,
          actions: actions,
        );
}
