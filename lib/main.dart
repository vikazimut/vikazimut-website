import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut_website/l10n/language_entity.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/url_routes/router_generator.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'constants.dart';
import 'l10n/l10n.dart';
import 'l10n/language_cubit.dart';
import 'theme/theme_cubit.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GoRouter.optionURLReflectsImperativeAPIs = true;
  Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  prefs.then((value) {
    String? languageCode = value.getString(KEY_LANGUAGE);
    String? theme = value.getString(KEY_THEME);
    runApp(VikazimutApp(languageCode, theme));
  });
}

class VikazimutApp extends StatelessWidget {
  final String? languageCode;
  final String? theme;

  const VikazimutApp(this.languageCode, this.theme);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ThemeCubit>(create: (context) => ThemeCubit(theme == null || theme == 'light' ? AppTheme.lightTheme : AppTheme.darkTheme)),
        BlocProvider<LanguageCubit>(create: (context) => LanguageCubit(languageCode == null ? null : Locale(languageCode!))),
      ],
      child: BlocBuilder<ThemeCubit, ThemeData>(builder: (context, theme) {
        return BlocBuilder<LanguageCubit, Locale?>(
          builder: (context, locale) {
            return MediaQuery.withClampedTextScaling(
              maxScaleFactor: ScreenHelper.isMobile(context) ? 0.8 : 1,
              child: MaterialApp.router(
                // useInheritedMediaQuery: true,
                title: "Vikazimut",
                scaffoldMessengerKey: globalKeyContext,
                locale: locale,
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  L10n.delegate,
                ],
                supportedLocales: Language.getSupportedLocales(),
                localeResolutionCallback: (Locale? deviceLocale, Iterable<Locale> supportedLocales) {
                  for (var locale in supportedLocales) {
                    if (locale.languageCode == deviceLocale?.languageCode) {
                      return locale;
                    }
                  }
                  return supportedLocales.first;
                },
                debugShowCheckedModeBanner: false,
                theme: theme,
                routerConfig: RouteGenerator.router,
              ),
            );
          },
        );
      }),
    );
  }
}
