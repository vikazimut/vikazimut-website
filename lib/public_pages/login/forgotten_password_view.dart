// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'login_page_view.dart';

class ResetPasswordView extends StatefulWidget {
  static const String routePath = '/reset-password';

  const ResetPasswordView();

  @override
  State<ResetPasswordView> createState() => ForgottenPasswordViewState();
}

class ForgottenPasswordViewState extends State<ResetPasswordView> {
  final TextEditingController _nameController = TextEditingController();
  late final _ResetPasswordPresenter _presenter;

  String? _errorMessage;
  bool _isMailSent = false;

  @override
  void initState() {
    _presenter = _ResetPasswordPresenter(this);
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: ScreenHelper.isDesktop(context) ? const EdgeInsets.only(top: 20) : const EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Column(
              children: [
                ContainerDecoration(
                  constraints: const BoxConstraints(maxWidth: 400),
                  child: Column(
                    children: [
                      Text(
                        L10n.getString("reset_password_page_title"),
                        style: Theme.of(context).textTheme.displayMedium,
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 30),
                      Text(L10n.getString("reset_password_page_text")),
                      const SizedBox(height: 30),
                      if (_errorMessage != null) ErrorContainer(_errorMessage!),
                      TextField(
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.next,
                        autofocus: true,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(userIdentifierLength),
                          FilteringTextInputFormatter.allow(userIdentifierRegExp),
                        ],
                        cursorColor: kOrangeColor,
                        controller: _nameController,
                        decoration: InputDecoration(
                          labelText: L10n.getString("reset_password_page_username_textfield_placeholder"),
                        ),
                      ),
                      const SizedBox(height: 15),
                      AnimatedBuilder(
                        animation: _nameController,
                        builder: (_, __) {
                          return SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: _nameController.text.isNotEmpty
                                  ? () {
                                      _presenter.sendResetPasswordRequest(_nameController.text);
                                      setState(() {
                                        _isMailSent = true;
                                        _errorMessage = null;
                                      });
                                    }
                                  : null,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: kOrangeColor,
                                padding: const EdgeInsets.all(20),
                              ),
                              child: Text(
                                (_isMailSent) ? L10n.getString("reset_password_page_mail_sent") : L10n.getString("reset_password_page_button_label"),
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 40),
                Footer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void redirect(String page) {
    GoRouter.of(context).replace(page);
  }

  void displayError(String? message) {
    setState(() {
      _isMailSent = false;
      _errorMessage = message == null ? null : L10n.getString(message);
    });
  }
}

class _ResetPasswordPresenter {
  final ForgottenPasswordViewState _view;

  const _ResetPasswordPresenter(this._view);

  Future<void> sendResetPasswordRequest(String username) async {
    try {
      final response = await http.patch(
        Uri.parse("${constants.API_SERVER}/reset-password"),
        body: jsonEncode({
          "username": username,
          "message": L10n.getString("mailer_reset_password"),
        }),
      );
      if (response.statusCode == HttpStatus.ok) {
        await Future.delayed(const Duration(seconds: 1));
        _view.displayError(null);
        _view.redirect(LoginPageView.routePath);
      } else {
        _view.displayError(jsonDecode(response.body));
      }
    } catch (error) {
      _view.displayError("Server connection error");
    }
  }
}
