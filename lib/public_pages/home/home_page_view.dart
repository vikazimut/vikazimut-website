// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/public_pages/home/shift_project_section.dart';

import 'about_us_section.dart';
import 'image_banner.dart';
import 'main_section.dart';
import 'partners_section.dart';

class HomePageView extends StatelessWidget {
  static const String routePath = '/home';
  static const String aboutUsRoutePath = '/about';
  final bool _isAboutSectionSelected;
  final ScrollController _scrollController = ScrollController();
  late final GlobalKey _aboutUsSectionWidgetKey = GlobalKey();

  HomePageView([this._isAboutSectionSelected = false]);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (_isAboutSectionSelected) {
        await Future.delayed(const Duration(milliseconds: 400));
        if (_aboutUsSectionWidgetKey.currentContext != null) {
          Scrollable.ensureVisible(
            _aboutUsSectionWidgetKey.currentContext!,
            duration: const Duration(milliseconds: 300),
          );
        }
      } else {
        if (_scrollController.offset != 0) {
          _scrollController.animateTo(
            0,
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeInOut,
          );
        }
      }
    });
    return ResponsiveScaffoldWidget(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Scrollbar(
                thumbVisibility: true,
                trackVisibility: true,
                controller: _scrollController,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ImageBanner(),
                      MainSection(),
                      AboutUsSection(key: _aboutUsSectionWidgetKey),
                      ShiftProjectSection(),
                      PartnersSection(),
                    ],
                  ),
                ),
              ),
            ),
            Footer(),
          ],
        ),
      ),
    );
  }
}
