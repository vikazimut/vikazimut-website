import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'key_performance_indicator/performance_processor.dart';

class OrienteeringPerformancePresenter {
  static Future<List<PerformanceIndicators>> computeKeypointIndicators(
    List<GpsRoute> selectedTracks,
    List<Checkpoint> checkpoints,
  ) async {
    List<PerformanceIndicators> performanceData = [];
    for (var track in selectedTracks) {
      PerformanceProcessor performanceProcessor = PerformanceProcessor();
      performanceData.add(performanceProcessor.computeKeypointIndicators(track, checkpoints));
    }
    return performanceData;
  }
}
