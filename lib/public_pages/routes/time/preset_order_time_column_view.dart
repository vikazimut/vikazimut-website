// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'time_column_helper.dart';
import 'time_sheet_presenter.dart';
import 'time_sheet_tab_view.dart';

class PresetOrderTimeColumnView extends StatelessWidget {
  final GpsRoute _selectedTrack;
  final TimeSheetPresenter _tableSheetPresenter;

  const PresetOrderTimeColumnView(
    this._selectedTrack,
    this._tableSheetPresenter,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          left: BorderSide(width: 3.0, color: kOrangeColor),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.person, color: _selectedTrack.color),
              Text(
                _selectedTrack.nickname,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          DataTable(
            border: TableBorder.all(
              style: BorderStyle.solid,
              width: 1,
              color: kOrangeColorDisabled,
            ),
            horizontalMargin: 10,
            columnSpacing: 5,
            headingRowHeight: TimeSheetTabView.HEADER_ROW_HEIGHT,
            dataRowMinHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
            dataRowMaxHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
            showBottomBorder: true,
            columns: [
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_time"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_distance"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_pace"))),
            ],
            rows: _buildRows(Theme.of(context), _selectedTrack.model),
          ),
        ],
      ),
    );
  }

  List<DataRow> _buildRows(ThemeData theme, TrackModel track) {
    List<DataRow> rows = [];
    var legLength = 0.0;
    var totalDistance = 0.0;
    var totalElevation = 0.0;
    var totalTime = 0;
    for (int legIndex = 1; legIndex < math.min(track.legCount, _tableSheetPresenter.length); legIndex++) {
      String timeCell;
      final String cumulativeTimeCell;
      final String distanceCell;
      final String paceCell;
      final punchTime = (legIndex < track.punchTimes.length) ? track.punchTimes[legIndex].timestampInMillisecond : -1;
      legLength += _tableSheetPresenter.getDistance(legIndex);
      if (punchTime > 0) {
        final elapsedPunchTime = track.calculateLegDuration(legIndex);
        final legDistance = track.calculateLegActualDistance(legIndex);
        final legElevationGain = track.calculateLegElevationGain(legIndex);
        timeCell = TimeColumnHelper.formatTime(elapsedPunchTime);
        cumulativeTimeCell = TimeColumnHelper.formatTime(punchTime);
        if (track.punchTimes[legIndex].forced) timeCell += "^";
        distanceCell = TimeColumnHelper.distanceDataToString(legDistance, legLength, legElevationGain);
        final pace = TimeColumnHelper.calculatePaceInMillisecondPerKm(elapsedPunchTime, legLength);
        final speed = TimeColumnHelper.calculateSpeedInKmh(elapsedPunchTime, legDistance);
        paceCell = '${TimeColumnHelper.formatTime(pace)}\n${TimeColumnHelper.formatSpeed(speed)}';
        totalDistance += legDistance;
        totalElevation += legElevationGain > 0 ? legElevationGain : 0;
        totalTime += elapsedPunchTime;
        legLength = 0;
      } else {
        timeCell = "--:--";
        cumulativeTimeCell = "--:--";
        distanceCell = "- (-)";
        paceCell = "--:--";
      }
      DataRow tableRow = DataRow(
        cells: [
          DataCell(Center(child: TextCentered("$timeCell\n$cumulativeTimeCell"))),
          DataCell(Center(child: TextCentered(distanceCell))),
          DataCell(Center(child: TextCentered(paceCell))),
        ],
        color: legIndex.isEven ? WidgetStateProperty.all(theme.colorScheme.inversePrimary) : WidgetStateProperty.all(theme.colorScheme.surface),
      );
      rows.add(tableRow);
    }
    // Totals
    final totalLength = _tableSheetPresenter.getActualRouteDistanceInMeters();
    String totalTimeCell = TimeColumnHelper.formatTime(totalTime);
    String totalDistanceCell = TimeColumnHelper.distanceDataToString(totalDistance, totalLength, totalElevation);
    final pace = TimeColumnHelper.calculatePaceInMillisecondPerKm(totalTime, totalLength);
    final speed = TimeColumnHelper.calculateSpeedInKmh(totalTime, totalDistance);
    String totalPaceCell = '${TimeColumnHelper.formatTime(pace)}\n${TimeColumnHelper.formatSpeed(speed)}';
    DataRow totalTableRow = DataRow(
      cells: [
        DataCell(TotalCell(child: TextCenteredBold(totalTimeCell))),
        DataCell(TotalCell(child: TextCenteredBold(totalDistanceCell))),
        DataCell(TotalCell(child: TextCenteredBold(totalPaceCell))),
      ],
    );
    rows.insert(0, totalTableRow);
    return rows;
  }
}
