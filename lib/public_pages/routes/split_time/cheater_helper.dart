import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'split_time_sheet_model.dart';
import 'time_cell.dart';

class CheaterHelper {
  static const int NUMBER_OF_CONSECUTIVE_LEGS = 4;
  static const int MIN_DIFFERENCE_IN_MS = 5 * 1000;
  static const int MIN_TIME_IN_MS = 1 * 60 * 1000;

  static void findCheater(List<List<TimeCell>> timeCells, List<Checkpoint> checkpoints, List<GpsRoute> selectedTracks) {
    // cheating: 4 legs in a row with same times
    for (int column = FIRST_SPLIT_COLUMN_INDEX; column <= timeCells[0].length - NUMBER_OF_CONSECUTIVE_LEGS; column++) {
      for (int row1 = 1; row1 < timeCells.length - 1; row1++) {
        for (int row2 = row1 + 1; row2 < timeCells.length; row2++) {
          final time11 = timeCells[row1][column].cumulativeTime;
          final time12 = timeCells[row1][column + 1].cumulativeTime;
          final time13 = timeCells[row1][column + 2].cumulativeTime;
          final time14 = timeCells[row1][column + 3].cumulativeTime;

          final time21 = timeCells[row2][column].cumulativeTime;
          final time22 = timeCells[row2][column + 1].cumulativeTime;
          final time23 = timeCells[row2][column + 2].cumulativeTime;
          final time24 = timeCells[row2][column + 3].cumulativeTime;

          if (isLessThanFiveSecondBetweenFourSuccessivePunchTimes_(
                time11,
                time12,
                time13,
                time14,
                time21,
                time22,
                time23,
                time24,
                MIN_DIFFERENCE_IN_MS,
              ) &&
              isGreaterThanOneMinuteForAllFourPunchTimes_(
                time11,
                time14,
                time21,
                time24,
                MIN_TIME_IN_MS,
              )) {
            timeCells[row1][0].isCheater = true;
            timeCells[row2][0].isCheater = true;
            break;
          }
        }
      }
    }
  }

  @visibleForTesting
  static bool isLessThanFiveSecondBetweenFourSuccessivePunchTimes_(int time11, int time12, int time13, int time14, int time21, int time22, int time23, int time24, int minDifferenceInMs) {
    return (time11 - time21).abs() < minDifferenceInMs && (time12 - time22).abs() < minDifferenceInMs && (time13 - time23).abs() < minDifferenceInMs && (time14 - time24).abs() < minDifferenceInMs;
  }

  @visibleForTesting
  static bool isGreaterThanOneMinuteForAllFourPunchTimes_(int time11, int time14, int time21, int time24, int minTimeInMs) {
    return (time14 - time11) > minTimeInMs || (time24 - time21) > minTimeInMs;
  }
}
