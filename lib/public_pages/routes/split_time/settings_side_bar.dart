// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/help_widget.dart';
import 'package:vikazimut_website/utils/html_utils.dart';

import 'export_split_time_as_csv.dart';
import 'split_time_sheet_presenter.dart';

class SettingsSideBar extends StatefulWidget {
  final SplitTimeSettings settings;
  final RouteListPagePresenter routePresenter;
  final SplitTimePresenter presenter;

  const SettingsSideBar(this.routePresenter, this.settings, this.presenter);

  @override
  State<SettingsSideBar> createState() => _SettingsSideBarState();
}

class _SettingsSideBarState extends State<SettingsSideBar> {
  static const String DEFAULT_VALUE = "50";
  final TextEditingController _scrollController = TextEditingController(text: DEFAULT_VALUE);
  final _focusNode = FocusNode();

  @override
  void initState() {
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        if (_scrollController.text.isEmpty) {
          _scrollController.text = DEFAULT_VALUE;
        }
        var numberInput = int.parse(_scrollController.text);
        setState(() {
          widget.settings.thresholdErrorRate.value = numberInput;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("time_sheet_split_settings"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(10.0),
      children: [
        Text(
          L10n.getString("time_sheet_split_settings1_title"),
        ),
        const SizedBox(height: 8),
        TextField(
          controller: _scrollController,
          focusNode: _focusNode,
          cursorColor: kOrangeColor,
          decoration: InputDecoration(
            suffixText: "%",
            labelText: L10n.getString("time_sheet_split_settings1_label"),
            suffixStyle: const TextStyle(color: kTextColorDarkTheme),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly,
            LengthLimitingTextInputFormatter(3),
          ],
        ),
        const SizedBox(height: 20),
        CustomOutlinedButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) => HelpWidget(
                L10n.getString("time_sheet_split_help_title"),
                L10n.getString("time_sheet_split_help_message"),
              ),
            );
          },
          label: L10n.getString("help_label"),
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
        ),
        SizedBox(
          width: 380,
          child: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: OutlinedButton(
              onPressed: () {
                downloadStringInFile("${widget.routePresenter.courseId}.csv", ExportSplitTimeAsCsv.convertToCsv(widget.presenter));
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: Text(
                      L10n.getString("time_sheet_split_export_button"),
                    ),
                  ),
                  const SizedBox(width: 5),
                  const Icon(Icons.file_download),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class SplitTimeSettings {
  final ValueNotifier<int> thresholdErrorRate = ValueNotifier(50);

  void dispose() {
    thresholdErrorRate.dispose();
  }
}
