// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'speed_color_caption_widget.dart';
import 'track_legs_sidebar.dart';
import 'track_tab_presenter.dart';

class TrackTabView extends StatefulWidget {
  final RouteListPagePresenter presenter;
  final Future<CourseData> courseInfo;

  const TrackTabView({
    required this.presenter,
    required this.courseInfo,
  });

  @override
  State<TrackTabView> createState() => _TrackTabViewState();
}

class _TrackTabViewState extends State<TrackTabView> with TickerProviderStateMixin, AutomaticKeepAliveClientMixin<TrackTabView> {
  late final TrackTabPresenter _trackTabPresenter;
  late final ScrollController _scrollController;
  bool _showedMap = true;
  MapZoomController? _mapZoomController;
  int _currentLeg = 0;

  @override
  void initState() {
    _trackTabPresenter = TrackTabPresenter();
    _mapZoomController = MapZoomController(this);
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _trackTabPresenter.dispose();
    _mapZoomController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder<CourseData>(
      future: widget.courseInfo,
      builder: (BuildContext context, AsyncSnapshot<CourseData> snapshot) {
        if (snapshot.hasData && snapshot.data != null && snapshot.connectionState == ConnectionState.done) {
          var selectedTracks = widget.presenter.selectedTracks;
          var overlayImages = <CustomBaseOverlayImage>[];
          List<TaggedColoredPolyline> polylines = [];
          List<List<LatLng>> allTracksAsPoints = widget.presenter.getAllTracksAsGpsPositions(legIndex: _currentLeg);
          List<List<PunchPosition>> allPunchPositions = widget.presenter.getAllPunchesAsPositions(legIndex: _currentLeg);
          List<List<int>> allSpeedsAsColorIndices = widget.presenter.getAllSpeedsAsColor(snapshot.data!.courseGeoreference.discipline, legIndex: _currentLeg);
          for (int i = 0; i < allTracksAsPoints.length; i++) {
            polylines.add(TaggedColoredPolyline(
              tag: "$i",
              points: allTracksAsPoints[i],
              punchPositions: allPunchPositions[i],
              pointColorIndices: allSpeedsAsColorIndices[i],
              color: selectedTracks[i].color,
              strokeWidth: 5,
            ));
          }
          var latLngBounds = snapshot.data!.courseGeoreference.latLngBounds();
          var rotation = -snapshot.data!.courseGeoreference.bounds.rotation;
          LatLngBounds bounds = LatLngBounds(LatLng(latLngBounds[3][0], latLngBounds[3][1]), LatLng(latLngBounds[1][0], latLngBounds[1][1]));
          _mapZoomController!.bounds = bounds;
          overlayImages = <CustomBaseOverlayImage>[
            CustomRotatedOverlayImage(
              bounds: bounds,
              opacity: 0.8,
              angleInDegree: rotation,
              imageProvider: loadNetworkImage(widget.presenter.user, snapshot.data!.courseGeoreference.imageId),
            ),
          ];
          final renderBox = context.findRenderObject() as RenderBox;
          return SizedBox(
            height: double.infinity,
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                _MapWithTracks(
                  mapController: _mapZoomController!.mapController,
                  trackTabPresenter: _trackTabPresenter,
                  bounds: bounds,
                  polylines: polylines,
                  startLatitude: snapshot.data!.courseGeoreference.startLatitude,
                  startLongitude: snapshot.data!.courseGeoreference.startLongitude,
                  showedMap: _showedMap,
                  overlayImages: overlayImages,
                ),
                Positioned(
                  left: constants.COLLAPSIBLE_MIN_WIDTH,
                  top: 0,
                  child: Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    width: renderBox.size.width - constants.COLLAPSIBLE_MIN_WIDTH,
                    padding: const EdgeInsets.all(3),
                    child: Row(
                      children: [
                        Text(
                          L10n.getString("track_leg_toolbar_title"),
                          style: const TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Scrollbar(
                            interactive: true,
                            thumbVisibility: true,
                            thickness: 4,
                            scrollbarOrientation: ScrollbarOrientation.bottom,
                            controller: _scrollController,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              controller: _scrollController,
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  _RadioButton(
                                    value: 0,
                                    groupValue: _currentLeg,
                                    title: "*",
                                    onChanged: (value) {
                                      _trackTabPresenter.updateLegStatistics(value!);
                                      setState(() => _currentLeg = value);
                                    },
                                  ),
                                  for (int i = 1; i < widget.presenter.courseData!.checkpoints.length; i++)
                                    _RadioButton(
                                      value: i,
                                      groupValue: _currentLeg,
                                      title: "$i",
                                      onChanged: (value) {
                                        _trackTabPresenter.updateLegStatistics(value!);
                                        setState(() => _currentLeg = value);
                                      },
                                    ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 40,
                  bottom: 20,
                  child: SpeedColorCaptionWidget(snapshot.data!.courseGeoreference.discipline, _trackTabPresenter),
                ),
                Positioned(
                  right: 5.0,
                  top: 55.0,
                  child: Column(
                    children: [
                      if (ScreenHelper.isDesktop(context))
                        _MapButton(
                          iconData: Icons.add,
                          onPressed: () => _mapZoomController?.zoomIn(),
                        ),
                      if (ScreenHelper.isDesktop(context))
                        _MapButton(
                          iconData: Icons.remove,
                          onPressed: () => _mapZoomController?.zoomOut(),
                        ),
                      if (!ScreenHelper.isMobile(context))
                        _MapButton(
                          iconData: Icons.my_location,
                          onPressed: () => _mapZoomController?.reset(),
                        ),
                      _MapButton(
                        onPressed: () => setState(() => _showedMap = !_showedMap),
                        iconData: (_showedMap) ? Icons.layers_clear : Icons.layers,
                      ),
                    ],
                  ),
                ),
                LegStatsSidebar(_trackTabPresenter, widget.presenter),
              ],
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _MapButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}

class _RadioButton extends StatelessWidget {
  final String title;
  final int value;
  final int groupValue;
  final ValueChanged<int?> onChanged;

  const _RadioButton({required this.title, required this.value, required this.groupValue, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    final isSelected = value == groupValue;
    return SizedBox(
      width: 20,
      height: 30,
      child: InkWell(
        onTap: () {
          onChanged(value);
        },
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isSelected ? kOrangeColor : null,
            border: Border.all(
              color: kOrangeColorDisabled,
            ),
          ),
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 10,
            ),
          ),
        ),
      ),
    );
  }
}

class _MapWithTracks extends StatefulWidget {
  final MapController mapController;
  final TrackTabPresenter trackTabPresenter;
  final LatLngBounds bounds;
  final List<TaggedColoredPolyline> polylines;
  final double startLatitude;
  final double startLongitude;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;

  const _MapWithTracks({
    required this.mapController,
    required this.trackTabPresenter,
    required this.bounds,
    required this.polylines,
    required this.startLatitude,
    required this.startLongitude,
    required this.showedMap,
    required this.overlayImages,
  });

  @override
  State<_MapWithTracks> createState() => _MapWithTracksState();
}

class _MapWithTracksState extends State<_MapWithTracks> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 19,
        minZoom: 10,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        if (widget.showedMap) CustomOverlayImageLayer(overlayImages: widget.overlayImages),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        CustomTappablePolylineLayer(
          pointerDistanceTolerance: 15,
          polylines: widget.polylines,
          onTap: (List<TaggedColoredPolyline> selectedPolylines, List<int> selectedSegments, TapUpDetails tapPosition) {
            if (selectedPolylines.isNotEmpty) {
              int index = int.parse(selectedPolylines.first.tag!);
              for (int i = 0; i < widget.polylines.length; i++) {
                if (i != index) {
                  widget.polylines[i].selected = false;
                }
              }
              widget.trackTabPresenter.selectLeg(widget.polylines[index]);
            }
            setState(() {});
          },
        ),
        MarkerLayer(
          markers: [
            Marker(
              alignment: Alignment.topCenter,
              width: 48.0,
              height: 48.0,
              point: LatLng(widget.startLatitude, widget.startLongitude),
              child: const Icon(
                Icons.location_pin,
                color: kVioline,
                size: 48,
              ),
            ),
          ],
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }
}
