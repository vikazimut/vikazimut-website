// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

import '../gps_route.dart';

const double _COLOR_INDICATOR_HEIGHT = 30.0;
const double _COLOR_INDICATOR_WIDTH = 30.0;

class PresetOrderOrienteerTile extends StatelessWidget {
  final GpsRoute track;
  final int rank;
  final int _loading;

  const PresetOrderOrienteerTile(this.track, this.rank, this._loading);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        children: [
          SizedBox(
            width: 15,
            child: Text(
              "$rank",
              style: const TextStyle(
                fontSize: 14,
                color: kOrangeColor,
              ),
            ),
          ),
          Flexible(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 12,
                      child: (track.model.isCheating)
                          ? Tooltip(
                              waitDuration: const Duration(seconds: 1),
                              message: L10n.getString("track_cheating_legend"),
                              child: Text(
                                "${track.nickname} *",
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            )
                          : Text(
                              track.nickname,
                              style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Text(
                        track.time,
                        style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Tooltip(
                        waitDuration: const Duration(seconds: 1),
                        message: L10n.getString("track_import_legend"),
                        child: Text(
                          (track.model.imported) ? "\u21E9" : "",
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text.rich(
                    textAlign: TextAlign.center,
                    TextSpan(
                      style: const TextStyle(fontSize: 13),
                      children: [
                        TextSpan(
                          text: L10n.getString("track_score_text1"),
                          style: TextStyle(color: Theme.of(context).hintColor),
                        ),
                        TextSpan(
                          text: track.model.countMissingPunches.toString(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: L10n.getString("track_score_text2"),
                          style: TextStyle(color: Theme.of(context).hintColor),
                        ),
                        TextSpan(
                          text: track.model.countForcedCheckpoints.toString(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          if (_loading == track.id)
            const SizedBox(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              child: CircularProgressIndicator(),
            )
          else
            Container(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              decoration: BoxDecoration(
                color: track.color,
                shape: BoxShape.circle,
              ),
            ),
        ],
      ),
    );
  }
}

class FreeOrderOrienteerTile extends StatelessWidget {
  final GpsRoute track;
  final int rank;
  final int _loading;

  const FreeOrderOrienteerTile(this.track, this.rank, this._loading);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            width: 15,
            child: Text(
              "${rank + 1}",
              style: const TextStyle(
                fontSize: 14,
                color: kOrangeColor,
              ),
            ),
          ),
          Flexible(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 12,
                      child: (track.model.isCheating)
                          ? Tooltip(
                              waitDuration: const Duration(seconds: 1),
                              message: L10n.getString("track_cheating_legend"),
                              child: Text(
                                "${track.nickname} *",
                                style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            )
                          : Text(
                              track.nickname,
                              style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Text(
                        track.time,
                        style: const TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Tooltip(
                        waitDuration: const Duration(seconds: 1),
                        message: L10n.getString("track_import_legend"),
                        child: Text(
                          (track.model.imported) ? "\u21E9" : "",
                          style: const TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text.rich(
                    textAlign: TextAlign.center,
                    TextSpan(
                      style: const TextStyle(
                        fontSize: 13,
                      ),
                      children: [
                        TextSpan(
                          text: L10n.getString("track_score_text3"),
                          style: TextStyle(color: Theme.of(context).hintColor),
                        ),
                        if (track.model.score >= 0)
                          TextSpan(
                            text: track.model.score.toString() + L10n.getString("track_score_text4"),
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        else
                          const TextSpan(
                            text: "--",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        if (track.model.runCount > 1)
                          TextSpan(
                            text: L10n.getString("track_score_text5"),
                            style: TextStyle(color: Theme.of(context).hintColor),
                          ),
                        if (track.model.runCount > 1)
                          TextSpan(
                            text: track.model.runCount.toString(),
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        if (track.model.runCount > 1)
                          TextSpan(
                            text: L10n.getString("track_score_text6"),
                            style: TextStyle(color: Theme.of(context).hintColor),
                          ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text.rich(
                    textAlign: TextAlign.center,
                    TextSpan(
                      style: const TextStyle(
                        fontSize: 13,
                      ),
                      children: [
                        TextSpan(
                          text: L10n.getString("track_score_text1"),
                          style: TextStyle(color: Theme.of(context).hintColor),
                        ),
                        TextSpan(
                          text: track.model.countMissingPunches.toString(),
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                        ),
                        TextSpan(
                          text: L10n.getString("track_score_text2"),
                          style: TextStyle(color: Theme.of(context).hintColor),
                        ),
                        TextSpan(
                          text: track.model.countForcedCheckpoints.toString(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          if (_loading == track.id)
            const SizedBox(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              child: CircularProgressIndicator(),
            )
          else
            Container(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              decoration: BoxDecoration(
                color: track.color,
                shape: BoxShape.circle,
              ),
            ),
        ],
      ),
    );
  }
}

class PresetOrderSupermanTile extends StatelessWidget {
  final GpsRoute track;
  final int rank;
  final int _loading;

  const PresetOrderSupermanTile(this.track, this.rank, this._loading);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      color: kOrangeColorDarkLight,
      child: Row(
        children: [
          Flexible(
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Image.asset(
                    'assets/images/superman48.png',
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(width: 5),
                Expanded(
                  flex: 10,
                  child: Tooltip(
                    message: L10n.getString("track_superman_tooltip"),
                    waitDuration: const Duration(seconds: 1),
                    child: Text(
                      track.nickname,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                (track.model.route != null && track.model.route!.isNotEmpty)
                    ? Expanded(
                        flex: 4,
                        child: Text(
                          track.time,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                      )
                    : const Expanded(
                        flex: 4,
                        child: Text(
                          "--:--:--",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                      ),
              ],
            ),
          ),
          const SizedBox(width: 8),
          if (_loading == track.id)
            const SizedBox(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              child: CircularProgressIndicator(),
            )
          else
            Container(
              height: _COLOR_INDICATOR_HEIGHT,
              width: _COLOR_INDICATOR_WIDTH,
              decoration: BoxDecoration(
                color: track.color,
                shape: BoxShape.circle,
              ),
            ),
        ],
      ),
    );
  }
}
