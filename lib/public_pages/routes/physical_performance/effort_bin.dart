class EffortBin {
  final int gradientInPercent;
  final double effortPercent;

  EffortBin(this.gradientInPercent, this.effortPercent);
}
