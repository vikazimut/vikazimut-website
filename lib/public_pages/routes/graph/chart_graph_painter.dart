// coverage:ignore-file
import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'graph_data.dart';
import 'settings_side_bar.dart';
import 'time_behind_leader_graph_presenter.dart';

class ChartGraphPainter extends CustomPainter {
  static const double _LEFT_MARGIN = 70.0;
  static const double _RIGHT_MARGIN = 30.0;
  static const double _TOP_MARGIN = 30.0;
  static const double _BOTTOM_MARGIN = 50.0;
  static const double _LINE_WIDTH = 2;

  final TimeBehindLeaderGraphPresenter _graphPresenter = TimeBehindLeaderGraphPresenter();
  late final GraphData _table;
  late final int _totalDistance;
  late final int _maxTimeDifference;

  ChartGraphPainter(RouteListPagePresenter presenter, GraphType graphType) {
    _table = _graphPresenter.buildTable(presenter.selectedTracks, presenter.courseData!.checkpoints, graphType);
    _totalDistance = TimeBehindLeaderGraphPresenter.getTotalDistance(_table);
    if (_graphPresenter.presetOrderSelectedTracks.length > 1) {
      _maxTimeDifference = TimeBehindLeaderGraphPresenter.calculateMaxTimeDifference(_table);
    } else {
      _maxTimeDifference = 1;
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    const TextStyle captionStyleTitle = TextStyle(color: kTextColorDarkTheme, fontSize: 18, fontWeight: FontWeight.bold);
    if (_graphPresenter.presetOrderSelectedTracks.isNotEmpty) {
      const TextStyle tickTextStyle = TextStyle(color: kTextColorDarkTheme, fontSize: 15);

      // draw x-axis caption
      var xAxisCaption = TextPainter(
        text: TextSpan(text: L10n.getString("time_behind_leader_graph_control"), style: captionStyleTitle),
        textDirection: TextDirection.ltr,
      );
      xAxisCaption.layout();
      xAxisCaption.paint(canvas, Offset(size.width / 2 - xAxisCaption.width / 2, size.height - xAxisCaption.height));

      // lines
      final solidPaint = Paint()
        ..color = kTextDarkColor
        ..strokeWidth = 0.8;
      final tickPaint = Paint()
        ..color = kTextDarkColor
        ..strokeWidth = 0.4;

      final bounds = [
        const Offset(_LEFT_MARGIN, _TOP_MARGIN),
        Offset(_LEFT_MARGIN, size.height - _BOTTOM_MARGIN),
        Offset(size.width - _RIGHT_MARGIN, size.height - _BOTTOM_MARGIN),
        Offset(size.width - _RIGHT_MARGIN, _TOP_MARGIN),
        const Offset(_LEFT_MARGIN, _TOP_MARGIN),
      ];
      var graphSize = Size(size.width - _LEFT_MARGIN - _RIGHT_MARGIN, size.height - _TOP_MARGIN - _BOTTOM_MARGIN);
      canvas.drawPoints(PointMode.polygon, bounds, solidPaint);
      List<double> xTicks = _getTicksXAxis(graphSize);
      _drawXAxisTicks(canvas, graphSize, solidPaint, tickTextStyle, tickPaint, xTicks);
      _drawYAxisTicks(canvas, graphSize, solidPaint, _maxTimeDifference, tickTextStyle, tickPaint);
      _drawTrackLines(canvas, graphSize, _maxTimeDifference, xTicks);

      // draw y-axis caption
      canvas.save;
      canvas.rotate(-math.pi / 2);
      var yAxisCaption = TextPainter(
        text: TextSpan(text: L10n.getString("time_behind_leader_graph_time_difference"), style: captionStyleTitle),
        textDirection: TextDirection.ltr,
      );
      yAxisCaption.layout();
      yAxisCaption.paint(canvas, Offset(-size.height / 2 - 50, -40));
      canvas.restore;
    }
  }

  List<double> _getTicksXAxis(Size size) {
    List<double> xTicks = [0];
    int cumulativeDistance = 0;
    for (int i = 0; i < _table.legDistance.length - 1; i++) {
      var distance = _table.legDistance[i + 1];
      cumulativeDistance += distance;
      xTicks.add(cumulativeDistance * size.width / _totalDistance);
    }
    return xTicks;
  }

  void _drawXAxisTicks(Canvas canvas, Size size, Paint solidPaint, TextStyle textStyle, Paint tickPaint, List<double> xTicks) {
    for (int i = 0; i < _table.legDistance.length; i++) {
      _drawCheckpointTick(xTicks[i], size, canvas, solidPaint, textStyle, i, tickPaint);
    }
  }

  void _drawCheckpointTick(double xPos, Size size, Canvas canvas, Paint solidPaint, TextStyle textStyle, int indexLegControl, Paint tickPaint) {
    // Draw tick vertical line
    var topOffset = Offset(xPos + _LEFT_MARGIN, _TOP_MARGIN);
    var bottomOffset = Offset(xPos + _LEFT_MARGIN, size.height + _TOP_MARGIN);
    canvas.drawCircle(bottomOffset, 2, solidPaint);
    canvas.drawLine(bottomOffset, topOffset, tickPaint);
    // Draw tick value
    String controlName;
    int heightOffset;
    if (indexLegControl == 0) {
      controlName = L10n.getString("time_behind_leader_graph_start");
      heightOffset = 14;
    } else if (indexLegControl == _table.data[0].length - 1) {
      controlName = L10n.getString("time_behind_leader_graph_finish");
      heightOffset = 14;
    } else {
      controlName = (indexLegControl).toString();
      heightOffset = 0;
    }
    var tickValuePainter = TextPainter(
      text: TextSpan(text: controlName, style: textStyle),
      textDirection: TextDirection.ltr,
    );
    tickValuePainter.layout();
    tickValuePainter.paint(canvas, Offset(xPos + _LEFT_MARGIN - tickValuePainter.size.width / 2, size.height + _TOP_MARGIN + heightOffset));
  }

  void _drawYAxisTicks(Canvas canvas, Size size, Paint solidPaint, int maxTimeDiff, TextStyle textStyle, Paint tickPaint) {
    if (_graphPresenter.presetOrderSelectedTracks.length <= 1) {
      _drawYAxisTickAndValue(0, 0, size, canvas, solidPaint, textStyle, tickPaint);
    } else {
      _drawYAxisTickAndValue(0, 0, size, canvas, solidPaint, textStyle, tickPaint);
      _drawYAxisTickAndValue(size.height * (1 / 4), (maxTimeDiff * (1 / 4)).round(), size, canvas, solidPaint, textStyle, tickPaint);
      _drawYAxisTickAndValue(size.height * (1 / 2), (maxTimeDiff * (1 / 2)).round(), size, canvas, solidPaint, textStyle, tickPaint);
      _drawYAxisTickAndValue(size.height * (3 / 4), (maxTimeDiff * (3 / 4)).round(), size, canvas, solidPaint, textStyle, tickPaint);
      _drawYAxisTickAndValue(size.height, maxTimeDiff, size, canvas, solidPaint, textStyle, tickPaint);
    }
  }

  void _drawYAxisTickAndValue(double yPos, int maxTimeDiff, Size size, Canvas canvas, Paint solidPaint, TextStyle textStyle, Paint tickPaint) {
    var offset = Offset(_LEFT_MARGIN, yPos + _TOP_MARGIN);
    canvas.drawCircle(offset, 2, solidPaint);
    canvas.drawLine(offset, Offset(size.width + _LEFT_MARGIN, yPos + _TOP_MARGIN), tickPaint);
    var textPainter = TextPainter(
      text: TextSpan(text: "+ ${timeToString(maxTimeDiff, withHour: true)}", style: textStyle),
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(_LEFT_MARGIN - textPainter.width - 5, yPos + _TOP_MARGIN - textPainter.height / 2));
  }

  void _drawTrackLines(Canvas canvas, Size size, int maxTimeDifference, List<double> xTicks) {
    Paint trackPaint = Paint()..strokeWidth = _LINE_WIDTH;
    for (int row = 0; row < _table.data.length; row++) {
      trackPaint.color = _table.colors[row]!;
      List<Offset> points = _buildTrackLine(row, size, maxTimeDifference, xTicks);
      canvas.drawPoints(PointMode.polygon, points, trackPaint);
    }
  }

  List<Offset> _buildTrackLine(int row, Size size, int maxTimeDifference, List<double> xTicks) {
    List<Offset> points = [];
    for (int column = 0; column < _table.data[0].length; column++) {
      int indexFirst = TimeBehindLeaderGraphPresenter.indexOfLeaderInTheColumn(_table.data, column);
      if (_table.data[row][column] != 0 || column == 0) {
        double yValue = ((_table.data[row][column] - _table.data[indexFirst][column]).abs() / maxTimeDifference);
        Offset point = Offset(xTicks[column] + _LEFT_MARGIN, yValue * size.height + _TOP_MARGIN);
        points.add(point);
      }
    }
    return points;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
