import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';

class SubTrack {
  static const int _LENGTH_OF_COMET_IN_MS = 60 * 1000;

  SubTrack._();

  static List<GeodesicPoint> getSubTrack(List<GeodesicPoint> track, int currentTime, int endTime) {
    if (currentTime >= endTime) {
      int currentGPSPointIndex = SubTrack.findCurrentGPSPoint_(track, endTime);
      var latitude = track[currentGPSPointIndex].latitude;
      var longitude = track[currentGPSPointIndex].longitude;
      return [GeodesicPoint(latitude, longitude, track[currentGPSPointIndex].timestampInMillisecond)];
    }
    int currentGPSPointIndex = SubTrack.findCurrentGPSPoint_(track, currentTime);
    return SubTrack.getSubList_(track, currentTime, currentGPSPointIndex);
  }

  @visibleForTesting
  static int findCurrentGPSPoint_(List<GeodesicPoint> track, int currentTime) {
    for (int i = 0; i < track.length; i++) {
      if (track[i].timestampInMillisecond >= currentTime) {
        return i;
      }
    }
    return 0;
  }

  @visibleForTesting
  static List<GeodesicPoint> getSubList_(List<GeodesicPoint> track, int currentTime, int currentTrackIndex) {
    if (track.length < 2 || currentTrackIndex >= track.length || currentTrackIndex <= 0) {
      return [];
    }
    var endTime = track[currentTrackIndex].timestampInMillisecond - _LENGTH_OF_COMET_IN_MS;
    var start = 0;
    for (int i = 0; i < track.length; i++) {
      if (track[i].timestampInMillisecond < endTime) {
        start = i;
      }
    }
    var subList = track.sublist(start, currentTrackIndex);
    // Add the last point at the specified time
    var t1 = track[currentTrackIndex - 1].timestampInMillisecond;
    var t2 = track[currentTrackIndex].timestampInMillisecond;
    double x = (currentTime - t1) / (t2 - t1);
    var latitude = x * track[currentTrackIndex].latitude + (1.0 - x) * track[currentTrackIndex - 1].latitude;
    var longitude = x * track[currentTrackIndex].longitude + (1.0 - x) * track[currentTrackIndex - 1].longitude;
    subList.add(GeodesicPoint(latitude, longitude, track[currentTrackIndex].timestampInMillisecond));
    return subList;
  }

  static bool isPunchLocation(int currentTime, List<PunchTime> punchTimes, int deltaTime) {
    for (int i = 0; i < punchTimes.length; i++) {
      final punchTime = punchTimes[i].timestampInMillisecond;
      double tmp = deltaTime / 4;
      if ((i == 0 || punchTime >= 0) && currentTime >= punchTime - tmp && currentTime <= punchTime + 3 * tmp) {
        return true;
      }
    }
    return false;
  }
}
