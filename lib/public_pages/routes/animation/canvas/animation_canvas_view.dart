// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/utils/map_coordinate_util.dart';

import '../animation_tab_presenter.dart';
import 'animated_route_layer.dart';

GlobalKey<AnimationCanvasState> _currentState = GlobalKey<AnimationCanvasState>();

class AnimationCanvas extends StatefulWidget {
  final Size imageSize;
  final LatLonBox terrainBounds;
  final List<AnimatedRouteLayer> _layers = [];
  final AnimationTabPresenter animationPresenter;

  AnimationCanvas({
    required this.imageSize,
    required this.terrainBounds,
    required this.animationPresenter,
  }) : super(key: _currentState) {
    animationPresenter.canvas = this;
  }

  @override
  AnimationCanvasState createState() => AnimationCanvasState();

  List<AnimatedRouteLayer> getLayers() => _layers;

  void addLayer(AnimatedRouteLayer layer) => _layers.add(layer);

  void removeAllLayers() => _layers.clear();

  Offset convertGeoPointIntoMapCoordinates(double latitude, double longitude) => _currentState.currentState!._convertGeoPointIntoMapCoordinates(latitude, longitude);

  void refresh() => _currentState.currentState!.refresh();
}

class AnimationCanvasState extends State<AnimationCanvas> {
  static const double MAX_SCALE = 10;
  static const double MIN_SCALE = 1;
  final TransformationController _transformationController = TransformationController();

  @override
  void dispose() {
    _transformationController.dispose();
    widget._layers.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _MapPainter(
              imageSize: widget.imageSize,
              layers: widget._layers,
              controller: _transformationController,
            ),
          ),
        ),
      ],
    );
  }

  void refresh() => setState(() {});

  Offset _convertGeoPointIntoMapCoordinates(double latitude, double longitude) {
    return MapCoordinateUtil.convertGeoPointIntoMapCoordinates(latitude, longitude, widget.terrainBounds, widget.imageSize.width.toInt(), widget.imageSize.height.toInt());
  }
}

class _MapPainter extends CustomPainter {
  final Size imageSize;
  final List<AnimatedRouteLayer> layers;
  final TransformationController controller;

  const _MapPainter({
    required this.imageSize,
    required this.layers,
    required this.controller,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final vScale = size.width / imageSize.width;
    final hScale = size.height / imageSize.height;
    final double scale = math.min(vScale, hScale);
    canvas.translate((size.width - imageSize.width * scale) / 2.0, (size.height - imageSize.height * scale) / 2.0);
    canvas.scale(scale);
    for (var layer in layers) {
      layer.draw(canvas, scale * controller.value.row0[0]);
    }
  }

  @override
  bool shouldRepaint(_MapPainter oldDelegate) => false;
}
