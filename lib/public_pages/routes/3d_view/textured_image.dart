import 'dart:async';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:image/image.dart' as img;

import 'course_3d_model.dart';
import 'hillshade_image.dart';
import 'image_size.dart';

class TexturedImage {
  static Future<img.Image> build(
    ui.Image orienteeringMap,
    List<double> normalizedAltitudes,
    ImageSize heightmapSize,
    double resolutionInMeterPerPixel,
    ImageSize newOrienteeringMapSize,
  ) async {
    img.Image orienteeringImage = await _convertFlutterUiToImage(orienteeringMap, newOrienteeringMapSize);
    List<int> hillshadeImage = HillshadeImage.generateMultidirectionalHillshade(normalizedAltitudes, heightmapSize.width, heightmapSize.height, resolutionInMeterPerPixel);
    _blendOrienteeringMapAndHillshade(orienteeringImage, hillshadeImage, heightmapSize.width, heightmapSize.height);
    return orienteeringImage;
  }

  static Future<img.Image> _convertFlutterUiToImage(ui.Image uiImage, ImageSize newOrienteeringMapSize) async {
    final ByteData? uiBytes = await uiImage.toByteData(format: ui.ImageByteFormat.rawRgba);
    var image = img.Image.fromBytes(width: uiImage.width, height: uiImage.height, bytes: uiBytes!.buffer, numChannels: 4);
    if (image.width > Course3dModel.MAX_IMAGE_SIDE_SIZE || image.height > Course3dModel.MAX_IMAGE_SIDE_SIZE) {
      // Added since mobile web navigator cannot reduce the size of images natively (see method course_3d_model.dart._loadImage()) [very long!]
      return img.copyResize(image, width: newOrienteeringMapSize.width.toInt(), height: newOrienteeringMapSize.height.toInt());
    } else {
      return image;
    }
  }

  static void _blendOrienteeringMapAndHillshade(
    img.Image mapImageAsImage,
    List<int> hillshade,
    int heightmapWidth,
    int heightmapHeight,
  ) {
    for (int y = 0; y < mapImageAsImage.height; y++) {
      for (int x = 0; x < mapImageAsImage.width; x++) {
        final int luminance = _getPixelFromHeightMap(hillshade, x, y, mapImageAsImage.width, mapImageAsImage.height, heightmapWidth, heightmapHeight);
        final double coefficient = _brighten(luminance / 255.0);
        final img.Pixel pixel = mapImageAsImage.getPixel(x, y);
        pixel.r = (pixel.r * coefficient).floor();
        pixel.g = (pixel.g * coefficient).floor();
        pixel.b = (pixel.b * coefficient).floor();
      }
    }
  }

  static double _brighten(double n) => math.sqrt(n * .8 + .2);

  static int _getPixelFromHeightMap(
    List<int> heightmap,
    int x,
    int y,
    int newWidth,
    int newHeight,
    int heightmapWidth,
    int heightmapHeight,
  ) {
    double linearInterpolation(double s, double e, double t) => s + (e - s) * t;
    double bilinearInterpolation(final double c00, double c10, double c01, double c11, double tx, double ty) => linearInterpolation(linearInterpolation(c00, c10, tx), linearInterpolation(c01, c11, tx), ty);

    final double gx = x / newWidth * (heightmapWidth - 1);
    final double gy = y / newHeight * (heightmapHeight - 1);
    final int gxi = gx.toInt();
    final int gyi = gy.toInt();
    final double c00 = heightmap[gyi * heightmapWidth + gxi].toDouble();
    final double c10 = heightmap[gyi * heightmapWidth + gxi + 1].toDouble();
    final double c01 = heightmap[(gyi + 1) * heightmapWidth + gxi].toDouble();
    final double c11 = heightmap[(gyi + 1) * heightmapWidth + gxi + 1].toDouble();
    return bilinearInterpolation(c00, c10, c01, c11, gx - gxi, gy - gyi).toInt();
  }
}
