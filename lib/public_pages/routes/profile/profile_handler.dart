import 'dart:ui';

import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/speed_color_handler.dart';

import 'decision_tree/decision_tree.dart';
import 'decision_tree/forest_decision_tree.dart';
import 'decision_tree/mtbo_decision_tree.dart';
import 'decision_tree/skio_decision_tree.dart';
import 'decision_tree/urban_decision_tree.dart';
import 'speed_histogram.dart';
import 'speed_in_kmh.dart';

class ProfileHandler {
  late final DecisionTree decisionTree;

  ProfileHandler(Discipline? discipline) {
    decisionTree = _init(discipline);
  }

  int get speedPerBin => decisionTree.speedPerBin;

  Future<List<ProfileData>> buildProfiles(List<GpsRoute> selectedTracks) async {
    List<ProfileData> profiles = [];
    for (int i = 0; i < selectedTracks.length; i++) {
      profiles.add(await buildProfile(selectedTracks[i].model));
    }
    return profiles;
  }

  Future<ProfileData> buildProfile(TrackModel track) async {
    List<SpeedInKmH> speedHistogram = _calculateSpeedHistogramFromWaypoints(track.route!, track.totalTimeInMilliseconds);
    OrienteerProfile profile = decisionTree.calculateOrienteeringLevel(speedHistogram);
    return ProfileData(
      track.nickname,
      speedHistogram,
      profile,
    );
  }

  static List<charts.Series<SpeedInKmH, String>> buildSpeedHistogramChart(List<SpeedInKmH> data, Discipline? discipline, int speedPerBin) {
    SpeedColorHandler speedColorHandler = SpeedColorHandlerFactory.build(discipline);
    return [
      charts.Series<SpeedInKmH, String>(
        id: 'Speed profile',
        colorFn: (SpeedInKmH item, __) => charts.ColorUtil.fromDartColor(
          Color(
            speedColorHandler.colorFromVelocity(speedPerBin * item.speed.toDouble()),
          ),
        ),
        domainFn: (SpeedInKmH item, _) => item.speed.toString(),
        measureFn: (SpeedInKmH item, _) => item.percent,
        data: data,
      )
    ];
  }

  List<SpeedInKmH> _calculateSpeedHistogramFromWaypoints(List<GeodesicPoint> waypoints, int totalTimeInMilliseconds) => SpeedHistogram.calculateSpeedHistogramFromWaypoints(waypoints, decisionTree.maxSpeed, totalTimeInMilliseconds);

  static DecisionTree _init(Discipline? discipline) {
    if (discipline == null) {
      return ForestDecisionTree();
    }
    switch (discipline) {
      case Discipline.FORESTO:
        return ForestDecisionTree();
      case Discipline.URBANO:
        return UrbanDecisionTree();
      case Discipline.MTBO:
        return MTBODecisionTree();
      case Discipline.SKIO:
        return SKIODecisionTree();
    }
  }
}

@immutable
class ProfileData {
  final String nickname;
  final List<SpeedInKmH> histogram;
  final OrienteerProfile profile;

  const ProfileData(this.nickname, this.histogram, this.profile);
}
