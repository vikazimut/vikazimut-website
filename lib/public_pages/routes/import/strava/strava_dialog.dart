// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'data/model_summary_activity.dart';

class StravaDialog {
  final Future<List<SummaryActivity>> _activityStream;

  StravaDialog(this._activityStream);

  Future<SummaryActivity?> execute(BuildContext context) async {
    return await showDialog<SummaryActivity?>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => _build(context),
    );
  }

  Widget _build(BuildContext context) {
    return AlertDialog(
      title: Text(
        L10n.getString("strava_dialog_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(maxHeight: 500),
        width: 400,
        child: Column(
          children: [
            Image.asset(
              'assets/images/api_logo_pwrdBy_strava_horiz_light.png',
            ),
            const Divider(thickness: 1),
            Expanded(
              child: FutureBuilder(
                future: _activityStream,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(child: Text(snapshot.error.toString()));
                  } else if (snapshot.hasData) {
                    final List<SummaryActivity> activities = snapshot.data!;
                    return Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.surface,
                        border: Border.all(width: 1, color: kCaptionColor),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListView.separated(
                        itemCount: activities.length,
                        itemBuilder: (context, index) {
                          final int dateInMilliseconds = convertISO8601DateToMilliseconds(activities[index].startDateLocal!);
                          return ListTile(
                            title: Text("${activities[index].name} (${getLocalizedDateWithTime(dateInMilliseconds)})"),
                            onTap: () {
                              Navigator.of(context).pop(activities[index]);
                            },
                          );
                        },
                        separatorBuilder: (context, index) {
                          return const Divider(thickness: 1);
                        },
                      ),
                    );
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(null),
          label: L10n.getString("button_cancel_label"),
        ),
      ],
    );
  }
}
