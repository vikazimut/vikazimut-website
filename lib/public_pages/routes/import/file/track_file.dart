import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import '../abstract_importer.dart';
import '../import_presenter.dart';
import 'track_file_format.dart';

class FileImporter extends AbstractImporter {
  @override
  Future<List<GeodesicPoint>?> getRouteWaypoints(BuildContext context, ImportPresenter presenter) async {
    FilePickerResult? file = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ["gpx", "fit"],
    );
    if (file != null) {
      try {
        PlatformFile gpxFile = file.files.first;
        String? error = _validate(gpxFile);
        if (error == null) {
          return TrackFileFormat.decoder.extractWaypoints(gpxFile.bytes!);
        } else {
          throw error;
        }
      } catch (error) {
        presenter.displayError(error);
      }
    }
    return null;
  }

  static String? _validate(PlatformFile? gpxFile) {
    if (gpxFile == null) {
      return "Empty file";
    }
    String? errorMessage = _checkContent(gpxFile);
    if (errorMessage != null) {
      return errorMessage;
    } else {
      return null;
    }
  }

  static String? _checkContent(PlatformFile trackFile) {
    if (!TrackFileFormat.isValidExtension(trackFile.extension)) {
      return L10n.getString("import_gpx_server_bad_file_extension", [trackFile.extension]);
    }
    TrackFileFormat trackFileFormatDecoder = TrackFileFormat.fromExtension(trackFile.extension!);
    String? errorMessage = trackFileFormatDecoder.checkContent(trackFile.bytes!);
    if (errorMessage != null) {
      return L10n.getString(errorMessage, [trackFile.name]);
    }
    return null;
  }
}
