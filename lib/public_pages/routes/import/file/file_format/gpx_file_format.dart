import 'dart:typed_data';

import 'package:vikazimut_website/public_pages/model/elevation_helper.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/utils/time.dart';
import 'package:xml/xml.dart';

import '../track_file_format.dart';

class GpxFileFormat implements TrackFileFormat {
  @override
  String? checkContent(Uint8List bytes) {
    String errorMessage = "import_gpx_server_bad_gpx_file";
    String xmlText = String.fromCharCodes(bytes);
    try {
      var gpxContents = xmlText.trim().toLowerCase();
      var indexOfXml = gpxContents.indexOf("<?xml");
      if (indexOfXml < 0) return errorMessage;

      var indexOfGpx = gpxContents.indexOf("<gpx");
      if (indexOfGpx <= 0) return errorMessage;

      gpxContents = gpxContents.substring(indexOfGpx);
      var indexOfTrk = gpxContents.indexOf("<trk>");
      if (indexOfTrk <= 0) return errorMessage;

      gpxContents = gpxContents.substring(indexOfTrk);
      var indexOfTrkseg = gpxContents.indexOf("<trkseg>");
      if (indexOfTrkseg <= 0) return errorMessage;

      gpxContents = gpxContents.substring(indexOfTrkseg);
      var indexOfTrkpt = gpxContents.indexOf("<trkpt");
      if (indexOfTrkpt <= 0) return errorMessage;

      gpxContents = gpxContents.substring(indexOfTrkpt);
      var indexOfTime = gpxContents.indexOf("<time>");
      if (indexOfTime <= 0) return errorMessage;

      return null;
    } catch (_) {
      return errorMessage;
    }
  }

  @override
  List<GeodesicPoint> extractWaypoints(Uint8List bytes) {
    String xmlText = String.fromCharCodes(bytes);
    return extractWaypoints_(xmlText);
  }

  List<GeodesicPoint> extractWaypoints_(String xmlText) {
    List<GeodesicPoint> waypoints = [];
    final XmlDocument document = XmlDocument.parse(xmlText);
    final total = document.findAllElements('trkseg');
    var baseTime = -1;
    for (var segment in total) {
      for (var waypoint in segment.findAllElements('trkpt')) {
        var timeString = waypoint.getElement('time');
        if (timeString == null) {
          continue;
        }
        var timestamp = convertISO8601DateToMilliseconds(waypoint.getElement('time')!.innerText);
        if (baseTime < 0) {
          baseTime = timestamp;
          timestamp = 0;
        } else {
          timestamp = timestamp - baseTime;
        }
        var latitude = double.parse(waypoint.getAttribute('lat')!);
        var longitude = double.parse(waypoint.getAttribute('lon')!);
        var elevationString = waypoint.getElement('ele');
        double altitude = (elevationString == null) ? ElevationHelper.NO_ELEVATION.toDouble() : double.parse(waypoint.getElement('ele')!.innerText);
        waypoints.add(GeodesicPoint(latitude, longitude, timestamp, altitude));
      }
    }
    return waypoints;
  }
}
