import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/utils/utils.dart';

class ImportModel {
  final Nickname _nickname = Nickname();
  final Format _format = Format();
  List<GeodesicPoint>? waypoints;

  set nickname(String? value) => _nickname.validate(value);

  String? get nickname => _nickname.value;

  set format(int value) => _format.validate(value);

  int get format => _format.value;

  bool get isValid => _nickname.isValid && _format.isValid && waypoints != null;

  void reset() {
    _nickname._value = "";
    _format._value = -1;
    waypoints = null;
  }
}

class Nickname {
  static final _regex = userIdentifierRegExp;
  String? _value;
  bool _isValid = false;

  String? get value => _value;

  bool get isValid => _isValid;

  bool validate(String? value) {
    if (value == null) {
      return false;
    }
    if (value.trim().isEmpty) {
      return false;
    }

    final Iterable<Match> matches = _regex.allMatches(value);
    if (matches.length == value.length) {
      _value = value;
      _isValid = true;
      return true;
    }
    return false;
  }
}

enum FormatValidationError { invalid }

class Format {
  int _value = -1;
  bool _isValid = false;

  int get value => _value;

  bool get isValid => _isValid;

  void validate(int value) {
    if (value > -1 && value < 2) {
      _value = value;
      _isValid = true;
    } else {
      _isValid = false;
    }
  }
}
