import 'package:flutter/foundation.dart';

@immutable
class Participant {
  final int id;
  final String nickname;
  final int rank;
  final int progress;
  final int totalPenalty;
  final int totalTime;
  final List<List<String>> eventCourses;
  final int totalScore; // Score total avec pénalités
  final int grossScore; // Score brut sans pénalités

  const Participant({
    required this.id,
    required this.nickname,
    required this.rank,
    required this.progress,
    required this.totalScore,
    required this.grossScore,
    required this.totalPenalty,
    required this.totalTime,
    required this.eventCourses,
  });

  factory Participant.fromJson(Map<String, dynamic> json) {
    return Participant(
      id: json["id"] as int,
      nickname: json["nickname"] as String,
      rank: json["rank"] as int,
      progress: json["progress"] as int,
      totalScore: json["totalScore"] as int,
      grossScore: json["grossScore"] as int,
      totalPenalty: json["totalPenalty"] as int,
      totalTime: json["totalTime"] as int,
      eventCourses: _readEventCoursesFromJson(json["eventCourse"]),
    );
  }

  static List<List<String>> _readEventCoursesFromJson(List<dynamic>? json) {
    if (json == null) return [];
    List<List<String>> eventCourses = [];
    for (int i = 0; i < json.length; i++) {
      List<dynamic> row = json[i];
      List<String> eventCourse = [];
      for (int j = 0; j < row.length; j++) {
        eventCourse.add('${row[j]}');
      }
      eventCourses.add(eventCourse);
    }
    return eventCourses;
  }
}
