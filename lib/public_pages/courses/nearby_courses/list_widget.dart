// coverage:ignore-file
part of 'nearby_courses_page_view.dart';

class _CustomListWidget extends StatelessWidget {
  final List<NearbyCourseModel> courses;
  final ScrollController? controller;
  final bool isFirstCourseSelected;

  const _CustomListWidget({
    required this.courses,
    required this.controller,
    required this.isFirstCourseSelected,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<IdentifiedUser?>(
      future: IdentifiedUser.getUser(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          bool isPlanner = snapshot.data != null;
          int startIndex = isFirstCourseSelected ? 1 : 0;
          return Column(
            children: [
              if (isFirstCourseSelected) ScreenHelper.isDesktop(context) ? _oneRow(context, courses[0], isPlanner) : _oneColumn(context, courses[0], isPlanner),
              Text(
                L10n.getString("nearby_courses_title"),
                style: Theme.of(context).textTheme.displayMedium,
              ),
              Expanded(
                child: Scrollbar(
                  thumbVisibility: true,
                  controller: controller,
                  child: ListView.builder(
                    controller: controller,
                    itemCount: courses.length - startIndex,
                    itemBuilder: (context, i) {
                      if (ScreenHelper.isDesktop(context)) {
                        return _oneRow(context, courses[i + startIndex], isPlanner);
                      } else {
                        return _oneColumn(context, courses[i + startIndex], isPlanner);
                      }
                    },
                  ),
                ),
              ),
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget _oneRow(BuildContext context, NearbyCourseModel course, bool isPlanner) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Theme.of(context).disabledColor),
      ),
      padding: const EdgeInsets.all(6),
      margin: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Discipline.getIcon(course.discipline),
            color: kOrangeColor,
          ),
          const SizedBox(width: 5),
          Expanded(flex: 4, child: _buildTitleColumn(course)),
          const SizedBox(width: 5),
          if (course.printable) _buildMapButton(course.id, context),
          const SizedBox(width: 5),
          _buildInfoButton(context, () => GoRouter.of(context).go("${CourseInfoViewPage.routePath}/${course.id}")),
          const SizedBox(width: 5),
          _buildTrackButton(course.tracks, () => GoRouter.of(context).go("${RouteListPageView.routePath}/${course.id}")),
          const SizedBox(width: 5),
          _buildLiveTrackingButton(context, course.id, isPlanner),
        ],
      ),
    );
  }

  Widget _oneColumn(BuildContext context, NearbyCourseModel course, bool isPlanner) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Theme.of(context).disabledColor),
      ),
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Discipline.getIcon(course.discipline),
                color: kOrangeColor,
              ),
              const SizedBox(width: 5),
              Expanded(child: _buildTitleColumn(course)),
              const SizedBox(width: 5),
            ],
          ),
          const SizedBox(height: 10),
          Wrap(
            direction: Axis.horizontal,
            alignment: WrapAlignment.center,
            runSpacing: 5,
            runAlignment: WrapAlignment.center,
            children: [
              if (course.printable) _buildMapButton(course.id, context),
              if (course.printable) const SizedBox(width: 5),
              _buildInfoButton(context, () => GoRouter.of(context).go("${CourseInfoViewPage.routePath}/${course.id}")),
              const SizedBox(width: 5),
              _buildTrackButton(course.tracks, () => GoRouter.of(context).go("${RouteListPageView.routePath}/${course.id}")),
              const SizedBox(width: 5),
              _buildLiveTrackingButton(context, course.id, isPlanner),
            ],
          ),
        ],
      ),
    );
  }

  Column _buildTitleColumn(NearbyCourseModel course) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          course.name,
          textAlign: TextAlign.left,
        ),
        if (course.touristic)
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              L10n.getString("nearby_courses_tourist_text"),
              style: const TextStyle(color: kSuccessColor, fontSize: 12),
            ),
          ),
        if (course.club != null) _buildSubtitle(course),
      ],
    );
  }

  Widget _buildSubtitle(NearbyCourseModel list) {
    return Align(
      alignment: Alignment.centerLeft,
      child: InkWell(
        onTap: (list.clubUrl != null) ? () => launchUrl(Uri.parse("${list.clubUrl}")) : null,
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 350, minWidth: 100),
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 2),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 2),
            decoration: BoxDecoration(
              color: (list.clubUrl != null) ? kOrangeColor : kOrangeColorDisabled,
              borderRadius: BorderRadius.circular(45),
            ),
            child: Center(
              child: Text(
                '${L10n.getString("nearby_courses_creation")} ${list.club}',
                style: TextStyle(
                  color: (list.clubUrl != null) ? Colors.white : Colors.black,
                  fontSize: 12,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMapButton(int id, BuildContext context) {
    return CustomPlainButton(
      label: L10n.getString("nearby_courses_map_button"),
      backgroundColor: kWarningColor,
      foregroundColor: Colors.black,
      onPressed: () {
        web.HTMLAnchorElement anchorElement = web.HTMLAnchorElement()..href = "${constants.SERVER}/data/$id/image";
        anchorElement.download = "Map image";
        anchorElement.click();
      },
    );
  }

  Widget _buildInfoButton(BuildContext context, void Function() onClick) {
    return CustomPlainButton(
      label: L10n.getString("nearby_courses_info_button"),
      backgroundColor: kInfoColor,
      onPressed: onClick,
    );
  }

  Widget _buildTrackButton(int tracks, void Function() onClick) {
    return CustomPlainButton(
      label: "${L10n.getString("nearby_courses_track_button")} ($tracks)",
      backgroundColor: kOrangeColor,
      onPressed: onClick,
    );
  }

  Widget _buildLiveTrackingButton(BuildContext context, int courseId, bool isPlanner) {
    if (!isPlanner) {
      return CustomPlainButton(
        label: L10n.getString("planner_course_list_page_button_live_tracking"),
        backgroundColor: kGreenColorLight,
        foregroundColor: Colors.black,
        onPressed: () {
          GoRouter.of(context).go("${LiveTrackingPage.routePathPublic}/$courseId");
        },
      );
    } else {
      return DropDownButton(
        buttonText: L10n.getString("planner_course_list_page_button_live_tracking"),
        itemTexts: [
          L10n.getString("live_tracking_creator_start_button_label"),
          L10n.getString("live_tracking_auditor_start_button_label"),
        ],
        backgroundColor: kGreenColorLight,
        foregroundColor: Colors.black,
        dense: false,
        onChanged: (int? value) {
          if (value == 0) {
            GoRouter.of(context).go("${LiveTrackingPage.routePathPlanner}/$courseId");
          } else {
            GoRouter.of(context).go("${LiveTrackingPage.routePathPublic}/$courseId");
          }
        },
      );
    }
  }
}
