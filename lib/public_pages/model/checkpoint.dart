import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

@immutable
class Checkpoint {
  final int index;
  final String id;
  final double latitude;
  final double longitude;
  final String type;
  final int score;

  const Checkpoint(
    this.index,
    this.id,
    this.latitude,
    this.longitude,
    this.type, [
    this.score = 1,
  ]);

  factory Checkpoint.fromJson(Map<String, dynamic> json) {
    return Checkpoint(
      json["index"] as int,
      json["id"],
      json["latitude"] as double,
      json["longitude"] as double,
      json["type"],
    );
  }

  static Future<List<Checkpoint>> loadCourseCheckpoints(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    final response = await http.get(
      Uri.parse("${constants.API_SERVER}/courses/checkpoints/$courseId"),
      headers: headers,
    );
    if (response.statusCode == HttpStatus.ok) {
      var data = jsonDecode(response.body);
      return addCheckpoints_(data);
    }
    return Future.error(response.body);
  }

  @visibleForTesting
  static List<Checkpoint> addCheckpoints_(List<dynamic> json) {
    List<Checkpoint> checkpoints = [];
    for (var data in json) {
      int index = data["index"];
      String id = data["id"];
      double latitude = data["location"][0];
      double longitude = data["location"][1];
      String type = data["type"];
      int score = data["score"];
      checkpoints.add(Checkpoint(index, id, latitude, longitude, type, score));
    }
    return checkpoints;
  }

  GeodesicPoint getLocation() {
    return GeodesicPoint(latitude, longitude);
  }
}
