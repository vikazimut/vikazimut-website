import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

import 'admin_planner_data.dart';

class AdminPlannerListGateway {
  static Future<List<AdminPlannerData>> fetchPlanners(String url) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse(url),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return (json.decode(response.body) as List).map((data) => AdminPlannerData.fromJson(data)).toList();
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> deletePlanner(String url) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }

  static Future<http.Response> addPlanner(String url, AdminPlannerData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.post(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: data.toJson(),
      );
      return response;
    }
  }

  static resetPassword(String url, int id) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      String messageInFrAndEn = "${L10n.getStringFromLanguage("fr", "mailer_reset_password")}\n\n${L10n.getStringFromLanguage("en", "mailer_reset_password")}";
      final response = await http.patch(Uri.parse("$url/$id"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: json.encode({"message": messageInFrAndEn}));
      return response;
    }
  }
}
