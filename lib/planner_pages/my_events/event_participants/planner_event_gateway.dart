import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import '../event_course_page/planner_event_course_model.dart';
import 'planner_event_participant_model.dart';

class PlannerEventGateway {
  static const String _GET_PARTICIPANTS_URL = "${constants.API_SERVER}/planner/my-event/%d/participants";
  static const String _GET_ORIENTEERS_URL = "${constants.API_SERVER}/courses/track-nicknames/%d";
  static const String _ADD_PARTICIPANTS_URL = "${constants.API_SERVER}/planner/my-event/%d/add-participants";
  static const String _MODIFY_PARTICIPANT_URL = "${constants.API_SERVER}/planner/my-event/%d/modify-participant";
  static const String _DELETE_PARTICIPANT_URL = "${constants.API_SERVER}/planner/my-event/%d/delete-participant/%d";

  static Future<List<PlannerEventParticipantModel>> fetchParticipants(int eventId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(sprintf(_GET_PARTICIPANTS_URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return (json.decode(response.body) as List).map((data) => PlannerEventParticipantModel.fromJson(data)).toList();
      } else {
        return Future.error(response.body);
      }
    }
  }

  static Future<Event> fetchCourses(int eventId) async {
    return PlannerEventCoursePageModel.fetchCourses(eventId);
  }

  static Future<http.Response> addParticipants(int eventId, List<String> nicknames) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      String jsonMessage = jsonEncode({"nicknames": nicknames});
      final response = await http.post(
        Uri.parse(sprintf(_ADD_PARTICIPANTS_URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: jsonMessage,
      );
      return response;
    }
  }

  static Future<http.Response> modifyParticipant(int eventId, String oldNickname, String nickname) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      String jsonMessage = jsonEncode({
        "newNickname": nickname,
        "oldNickname": oldNickname,
      });
      final response = await http.post(
        Uri.parse(sprintf(_MODIFY_PARTICIPANT_URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: jsonMessage,
      );
      return response;
    }
  }

  static Future<http.Response> deleteEvent(int eventId, int participantId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse(sprintf(_DELETE_PARTICIPANT_URL, [eventId, participantId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }

  static Future<List<String>> getListOfTrackFromCourse(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(sprintf(_GET_ORIENTEERS_URL, [courseId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return List<String>.from(json.decode(response.body));
      } else {
        return Future.error(response.body);
      }
    }
  }
}
