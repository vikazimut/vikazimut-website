// coverage:ignore-file
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/dropdown_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import '../event_course_page/planner_event_course_model.dart';
import '../event_course_page/planner_event_course_view.dart';
import '../event_detail_page/planner_event_detail_page.dart';
import 'course_selector.dart';
import 'nickname_editor.dart';
import 'planner_event_participant_model.dart';
import 'planner_event_participant_presenter.dart';

class PlannerEventParticipantView extends StatefulWidget {
  static const String routePath = '/planner/events/participants';
  final int eventId;

  PlannerEventParticipantView(args) : eventId = int.parse(args);

  @override
  State<PlannerEventParticipantView> createState() => PlannerEventParticipantViewState();
}

class PlannerEventParticipantViewState extends State<PlannerEventParticipantView> {
  final ScrollController _scrollController = ScrollController();
  Future<List<PlannerEventParticipantModel>>? _participants;
  late final PlannerEventParticipantPresenter _presenter;
  String? _errorMessage;

  @override
  void initState() {
    _presenter = PlannerEventParticipantPresenter(this);
    _participants = _presenter.fetchParticipants(widget.eventId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void update([dynamic body = ""]) {
    setState(() {
      _participants = _presenter.fetchParticipants(widget.eventId);
      _scrollController.jumpTo(0);
      if (body.isNotEmpty) {
        _errorMessage = L10n.getString("event_info_participants_register_error", [body]);
      } else {
        _errorMessage = null;
      }
    });
  }

  void error(String message) {
    setState(() {
      _errorMessage = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Container(
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            Flexible(
              child: ContainerDecoration(
                constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        L10n.getString("planner_event_participants_page_title"),
                        style: Theme.of(context).textTheme.displayMedium,
                      ),
                    ),
                    const SizedBox(height: 10),
                    FutureBuilder<List<PlannerEventParticipantModel>>(
                      future: _participants,
                      builder: (context, AsyncSnapshot<List<PlannerEventParticipantModel>> snapshot) {
                        if (snapshot.hasError) {
                          return Expanded(child: GlobalErrorWidget(snapshot.error.toString()));
                        } else if (!snapshot.hasData) {
                          return const Expanded(child: Center(child: CircularProgressIndicator()));
                        } else {
                          return Expanded(
                            child: Scrollbar(
                              controller: _scrollController,
                              thumbVisibility: true,
                              child: ListView.builder(
                                padding: const EdgeInsets.only(right: 8),
                                controller: _scrollController,
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, i) {
                                  int index = snapshot.data!.length - i - 1;
                                  return _ParticipantTile(widget.eventId, snapshot.data![index].id, snapshot.data![index].name, _presenter);
                                },
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 12),
            DropDownButton(
              buttonText: L10n.getString("planner_event_participants_page_button_add_participants"),
              itemTexts: [
                L10n.getString("planner_event_participants_page_button_add_participant"),
                L10n.getString("planner_event_participants_page_button_add_list_participants"),
                L10n.getString("planner_event_participants_page_button_add_orienteers"),
              ],
              backgroundColor: kOrangeColor,
              foregroundColor: Colors.white,
              dense: false,
              onChanged: (int? value) async {
                if (value == 0) {
                  String? nickname = await NicknameEditor().execute(context);
                  if (nickname != null) {
                    _presenter.addParticipants(widget.eventId, [nickname]);
                  }
                } else if (value == 1) {
                  FilePickerResult? file = await FilePicker.platform.pickFiles(
                    type: FileType.custom,
                    allowedExtensions: ["txt"],
                  );
                  if (file != null) {
                    _presenter.addParticipantsFromFile(widget.eventId, file.files.first);
                  }
                } else {
                  final Future<Event> courseEvents = _presenter.fetchEventCourses(widget.eventId);
                  int? courseId = await CourseSelector(courseEvents).execute(context);
                  if (courseId != null) {
                    _presenter.addParticipantsOfCourse(courseId, widget.eventId);
                  }
                }
              },
            ),
            const SizedBox(height: 5),
            Wrap(
              runSpacing: 5,
              direction: Axis.horizontal,
              alignment: WrapAlignment.center,
              spacing: 5,
              children: [
                const SizedBox(height: 15),
                CustomPlainButton(
                  label: L10n.getString("planner_event_list_page_button_detail"),
                  backgroundColor: kSuccessColor,
                  onPressed: () => GoRouter.of(context).go("${PlannerEventDetailPage.routePath}/${widget.eventId}"),
                ),
                CustomPlainButton(
                  label: L10n.getString("planner_event_list_page_button_courses"),
                  backgroundColor: kInfoColor,
                  onPressed: () => GoRouter.of(context).go("${PlannerEventCourseView.routePath}/${widget.eventId}"),
                ),
              ],
            ),
            if (_errorMessage != null) ErrorContainer(_errorMessage!),
            if (!ScreenHelper.isMobile(context)) const SizedBox(height: 8),
            Footer(),
          ],
        ),
      ),
    );
  }
}

class _ParticipantTile extends StatefulWidget {
  final int id;
  final int eventId;
  final String nickname;
  final PlannerEventParticipantPresenter presenter;

  const _ParticipantTile(this.eventId, this.id, this.nickname, this.presenter);

  @override
  State<_ParticipantTile> createState() => _ParticipantTileState();
}

class _ParticipantTileState extends State<_ParticipantTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: Theme.of(context).dialogTheme.backgroundColor,
          border: Border.all(width: 1, color: kCaptionColor),
        ),
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(widget.nickname),
            const SizedBox(width: 10),
            Row(
              children: [
                IconButton(
                  splashRadius: 20,
                  style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(kInfoColor),
                    foregroundColor: WidgetStateProperty.all(Colors.white),
                  ),
                  onPressed: () async {
                    String oldNickname = widget.nickname;
                    String? nickname = await NicknameEditor(widget.nickname).execute(context);
                    if (nickname != null && nickname != oldNickname) {
                      widget.presenter.modifyParticipant(widget.eventId, oldNickname, nickname);
                    }
                  },
                  icon: const Icon(Icons.edit),
                ),
                const SizedBox(width: 5),
                IconButton(
                  splashRadius: 20,
                  style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all(kDangerColor),
                    foregroundColor: WidgetStateProperty.all(Colors.white),
                  ),
                  onPressed: () => showBinaryQuestionAlertDialog(
                    context: context,
                    title: L10n.getString("planner_event_participants_page_delete_title"),
                    message: L10n.getString("planner_event_participants_page_delete_message", [widget.nickname]),
                    okMessage: L10n.getString("planner_event_participants_page_delete_ok_message"),
                    noMessage: L10n.getString("planner_event_participants_page_delete_no_message"),
                    action: () {
                      widget.presenter.deleteParticipant(widget.eventId, widget.id);
                    },
                  ),
                  icon: const Icon(Icons.delete),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
