// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';

class PlannerCourseMissingCheckpointsPage extends StatefulWidget {
  static const String routePath = '/planner/missing-checkpoints';
  final int courseId;

  PlannerCourseMissingCheckpointsPage(args) : courseId = int.parse(args);

  @override
  State<PlannerCourseMissingCheckpointsPage> createState() => _PlannerCourseMissingCheckpointsPageState();
}

class _PlannerCourseMissingCheckpointsPageState extends State<PlannerCourseMissingCheckpointsPage> {
  late final _PlannerCourseMissingCheckpointsPagePresenter _presenter;
  Future<List<_MissingCheckpoint>>? _missingCheckpoints;
  final ScrollController _scrollController = ScrollController();
  String? _errorMessage;

  @override
  void initState() {
    _presenter = _PlannerCourseMissingCheckpointsPagePresenter(this);
    _missingCheckpoints = _presenter.fetchMissingCheckpoints(widget.courseId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: 800),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("planner_missing_checkpoints_list_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                  const SizedBox(height: 10),
                  FutureBuilder<List<_MissingCheckpoint>>(
                    future: _missingCheckpoints,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return Expanded(child: GlobalErrorWidget(snapshot.error.toString()));
                      } else if (!snapshot.hasData) {
                        return const Expanded(child: Center(child: CircularProgressIndicator()));
                      } else {
                        if (snapshot.data!.isEmpty) {
                          return Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(width: 1, color: kCaptionColor),
                              ),
                              child: Center(
                                child: Text(L10n.getString("planner_missing_checkpoints_list_page_no_missing")),
                              ),
                            ),
                          );
                        } else {
                          return Expanded(
                            child: Scrollbar(
                              controller: _scrollController,
                              thumbVisibility: true,
                              child: ListView.builder(
                                controller: _scrollController,
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, i) {
                                  return Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                                        child: Text(
                                          snapshot.data![i].checkpointId,
                                          style: Theme.of(context).textTheme.displaySmall!.copyWith(color: kOrangeColor),
                                        ),
                                      ),
                                      CustomPlainButton(
                                        label: L10n.getString("planner_missing_checkpoints_list_page_button"),
                                        backgroundColor: kDangerColor,
                                        onPressed: () => showBinaryQuestionAlertDialog(
                                          context: context,
                                          title: L10n.getString("planner_missing_checkpoints_list_page_delete_title"),
                                          message: L10n.getString("planner_missing_checkpoints_list_page_delete_message"),
                                          okMessage: L10n.getString("planner_missing_checkpoints_list_page_delete_ok_message"),
                                          noMessage: L10n.getString("planner_missing_checkpoints_list_page_delete_no_message"),
                                          action: () {
                                            _presenter.deleteMissingCheckpoint(snapshot.data![i].id);
                                          },
                                        ),
                                      ),
                                      if (_errorMessage != null) ErrorContainer(_errorMessage!),
                                    ],
                                  );
                                },
                              ),
                            ),
                          );
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _missingCheckpoints = _presenter.fetchMissingCheckpoints(widget.courseId);
    });
  }

  void error(String message) {
    setState(() {
      _errorMessage = L10n.getString(message);
    });
  }
}

class _PlannerCourseMissingCheckpointsPagePresenter {
  static const String _URL_GET = '${constants.API_SERVER}/planner/my-course/missing-checkpoints';
  static const String _URL_DELETE = '${constants.API_SERVER}/planner/my-course/delete-missing-checkpoint/%d';
  final _PlannerCourseMissingCheckpointsPageState view;

  const _PlannerCourseMissingCheckpointsPagePresenter(this.view);

  Future<List<_MissingCheckpoint>> fetchMissingCheckpoints(int courseId) async {
    return _PlannerEventListPageModel.fetchCourses("$_URL_GET/$courseId");
  }

  Future<void> deleteMissingCheckpoint(int checkpointId) async {
    http.Response response = await _PlannerEventListPageModel.deleteMissingCheckpoint(sprintf(_URL_DELETE, [checkpointId]));
    if (response.statusCode == HttpStatus.ok) {
      view.update();
    } else {
      view.error(response.body);
    }
  }
}

class _PlannerEventListPageModel {
  static Future<List<_MissingCheckpoint>> fetchCourses(String url) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return (json.decode(response.body) as List).map((data) => _MissingCheckpoint.fromJson(data)).toList();
      } else {
        return Future.error(response.body);
      }
    }
  }

  static Future<http.Response> deleteMissingCheckpoint(String url) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }
}

class _MissingCheckpoint {
  final int id;
  final String checkpointId;

  const _MissingCheckpoint({
    required this.id,
    required this.checkpointId,
  });

  factory _MissingCheckpoint.fromJson(Map<String, dynamic> json) {
    return _MissingCheckpoint(
      id: json["id"] as int,
      checkpointId: json["checkpointId"] as String,
    );
  }
}
