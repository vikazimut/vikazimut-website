import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';

class PlannerCourseDataModel {
  static const String _GET_URL = "${constants.API_SERVER}/planner/my-course/data";
  static const String _POST_ADD_URL = "${constants.API_SERVER}/planner/my-course/upload-course";

  String name;
  int? courseType;
  int? courseFormat;
  int? courseMode;
  List<Checkpoint> checkpoints;

  factory PlannerCourseDataModel.fromJson(int courseId, dynamic json, List<Checkpoint> checkpoints) {
    var detectionRadius = json["detection_radius"] as int?;
    if (detectionRadius != null && (detectionRadius < 5 || detectionRadius > 55)) {
      detectionRadius = null;
    }
    return PlannerCourseDataModel(
      name: json["name"],
      courseType: json["course_type"] as int?,
      courseFormat: json["course_format"] as int?,
      courseMode: json["course_mode"] as int?,
      checkpoints: checkpoints,
    );
  }

  dynamic toJson() {
    return json.encode({
      "name": name,
      "club_name": null,
      "club_url": null,
      "start_date": null,
      "end_date": null,
      "is_printable": null,
      "detection_radius": null,
      "discipline": null,
      "course_type": courseType,
      "course_format": courseFormat,
      "course_mode": courseMode,
      "secret_key": null,
      "xml": null,
      "kml": null,
      "image": null,
      "selected_xml_course": null,
    });
  }

  PlannerCourseDataModel({
    required this.name,
    this.courseType,
    this.courseFormat,
    this.courseMode,
    required this.checkpoints,
  });

  static Future<PlannerCourseDataModel> fetchCourseData({required int courseId}) async {
    try {
      var checkpoints = await Checkpoint.loadCourseCheckpoints(courseId);
      return await _fetchCourseSettings(courseId: courseId, checkpoints: checkpoints);
    } catch (e) {
      return Future.error(e);
    }
  }

  static Future<String?> sendToServer(int courseId, PlannerCourseDataModel data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return "Access token error";
    } else {
      var request = http.MultipartRequest("POST", Uri.parse("$_POST_ADD_URL/$courseId"));
      request.headers.addAll({
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      });
      request.fields['data'] = data.toJson();
      try {
        var response = await http.Response.fromStream(await request.send());
        if (response.statusCode == HttpStatus.ok) {
          return null;
        } else {
          return "Server connection error";
        }
      } catch (_) {
        return "Server connection error";
      }
    }
  }

  static Future<PlannerCourseDataModel> _fetchCourseSettings({required int courseId, required List<Checkpoint> checkpoints}) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse("$_GET_URL/$courseId"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return PlannerCourseDataModel.fromJson(courseId, json.decode(response.body), checkpoints);
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }
}
