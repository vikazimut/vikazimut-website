import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/diacritic.dart';

import '../detail/community_course_stats_view.dart';
import 'community_data.dart';
import 'community_list_presenter.dart';

class CommunityDataTable extends StatefulWidget {
  final List<CommunityData> items;
  final CommunityListPagePresenter presenter;
  final GlobalKey<PaginatedDataTableState> tableKey;

  const CommunityDataTable(this.tableKey, this.presenter, this.items);

  @override
  State<CommunityDataTable> createState() => CommunityDataTableState();
}

class CommunityDataTableState extends State<CommunityDataTable> {
  final TextEditingController _searchFieldController = TextEditingController();
  _CommunityDataSource? _communityDataSource;
  late List<CommunityData> _initialCommunityList;
  static String _currentSearchCommunityName = "";

  @override
  void initState() {
    _initialCommunityList = widget.items.toList(growable: false);
    _searchFieldController.text = _currentSearchCommunityName;
    _communityDataSource = _CommunityDataSource(widget.presenter, _initialCommunityList, _currentSearchCommunityName, context: context);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant CommunityDataTable oldWidget) {
    _initialCommunityList = widget.items.toList(growable: false);
    _communityDataSource = _CommunityDataSource(widget.presenter, _initialCommunityList, _currentSearchCommunityName, context: context);
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _searchFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final int rowPerPage = (height - 360) ~/ 45;
    return Theme(
      data: Theme.of(context).copyWith(
        textTheme: const TextTheme(bodySmall: TextStyle(color: kOrangeColor)),
        cardTheme: CardTheme(color: Theme.of(context).scaffoldBackgroundColor),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      child: PaginatedDataTable(
        key: widget.tableKey,
        initialFirstRowIndex: CommunityListPagePresenter.currentDisplayedRowIndex,
        header: Padding(
          padding: const EdgeInsets.all(3),
          child: TextField(
            controller: _searchFieldController,
            style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: const BorderSide(
                  width: 2,
                  color: kOrangeColor,
                ),
              ),
              labelText: L10n.getString("community_list_page_search_hint_text"),
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
            ),
            onChanged: (searchText) {
              setState(() {
                _currentSearchCommunityName = searchText;
                widget.tableKey.currentState!.pageTo(0);
                _communityDataSource = _CommunityDataSource(widget.presenter, _initialCommunityList, searchText, context: context);
              });
            },
          ),
        ),
        showEmptyRows: false,
        dataRowMaxHeight: 45,
        dataRowMinHeight: 45,
        columnSpacing: 10.0,
        horizontalMargin: 0,
        rowsPerPage: rowPerPage,
        showFirstLastButtons: true,
        showCheckboxColumn: false,
        arrowHeadColor: kOrangeColor,
        onPageChanged: (int value) => CommunityListPagePresenter.currentDisplayedRowIndex = value,
        columns: [
          DataColumn(
            label: Expanded(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                child: Text(
                  L10n.getString("community_list_page_table_column1"),
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          DataColumn(
            numeric: true,
            label: Expanded(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                child: Text(
                  L10n.getString("community_list_page_table_column2"),
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
        source: _communityDataSource!,
      ),
    );
  }
}

class _CommunityDataSource extends DataTableSource {
  late final List<CommunityData> _communityList;
  final BuildContext context;
  final CommunityListPagePresenter presenter;

  _CommunityDataSource(this.presenter, List<CommunityData> communityList, String searchText, {required this.context}) {
    if (searchText.isNotEmpty) {
      var rawSearchText = searchText.toLowerCase().toRawString();
      _communityList = communityList.where((element) => element.name.toLowerCase().toRawString().contains(rawSearchText)).toList();
    } else {
      _communityList = communityList;
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _communityList.length;

  @override
  int get selectedRowCount => 0;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0 && index < _communityList.length);
    final CommunityData community = _communityList[index];
    return DataRow(color: index.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface), cells: [
      DataCell(
        TextButton(
          onPressed: () {
            GoRouter.of(context).go("${CommunityCourseStatsView.routePath}/?token=${community.encryptName()}");
          },
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.surface,
                  border: Border.all(color: kOrangeColor, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.query_stats,
                  color: Theme.of(context).colorScheme.tertiary,
                ),
              ),
              const SizedBox(width: 15),
              Text(
                community.name,
                style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
              ),
            ],
          ),
        ),
      ),
      DataCell(
        Center(
          child: SizedBox(
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                String url = "${constants.SERVER}/web/#${CommunityCourseStatsView.routePath}/?token=${community.encryptName()}";
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          L10n.getString("community_list_page_link_description"),
                        ),
                        Text(
                          url,
                          style: const TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                    actions: [
                      CustomPlainButton(
                        onPressed: () {
                          Clipboard.setData(ClipboardData(text: url));
                          Navigator.pop(context);
                        },
                        label: L10n.getString("community_list_page_url_button_ok"),
                        backgroundColor: kOrangeColor,
                      )
                    ],
                  ),
                );
              },
              style: ButtonStyle(
                backgroundColor: WidgetStateProperty.all(kWarningColor),
                padding: WidgetStateProperty.all<EdgeInsets?>(EdgeInsets.zero),
              ),
              child: Text(
                L10n.getString("community_list_page_url_button_copy"),
                style: const TextStyle(color: Colors.black),
              ),
            ),
          ),
        ),
      ),
    ]);
  }
}
