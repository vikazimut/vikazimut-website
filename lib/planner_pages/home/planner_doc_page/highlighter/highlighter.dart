import 'languages/all.dart';
import 'highlight.dart';

export 'highlight.dart';
export 'node.dart';
export 'mode.dart';
export 'result.dart';

final highlight = Highlight()..registerLanguages(allLanguages);
