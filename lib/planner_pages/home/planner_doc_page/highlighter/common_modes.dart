// GENERATED CODE - DO NOT MODIFY BY HAND

import 'mode.dart';

final kBackslashEscape = Mode(begin: "\\\\[\\s\\S]", relevance: 0);
final kAposStringMode = Mode(className: "string", begin: "'", end: "'", illegal: "\\n", contains: [Mode(begin: "\\\\[\\s\\S]", relevance: 0)]);
final kQuoteStringMode = Mode(className: "string", begin: "\"", end: "\"", illegal: "\\n", contains: [Mode(begin: "\\\\[\\s\\S]", relevance: 0)]);
final kPhrasalWordsMode = Mode(begin: "\\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\\b");
final kCLineCommentMode = Mode(className: "comment", begin: "//", end: "\$", contains: [Mode(begin: "\\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\\b"), Mode(className: "doctag", begin: "(?:TODO|FIXME|NOTE|BUG|XXX):", relevance: 0)]);
final kCBlockCommentMode = Mode(className: "comment", begin: "/\\*", end: "\\*/", contains: [Mode(begin: "\\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\\b"), Mode(className: "doctag", begin: "(?:TODO|FIXME|NOTE|BUG|XXX):", relevance: 0)]);
final kHashCommentMode = Mode(className: "comment", begin: "#", end: "\$", contains: [Mode(begin: "\\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\\b"), Mode(className: "doctag", begin: "(?:TODO|FIXME|NOTE|BUG|XXX):", relevance: 0)]);
final kNumberMode = Mode(className: "number", begin: "\\b\\d+(\\.\\d+)?", relevance: 0);
final kCNumberMode = Mode(className: "number", begin: "(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", relevance: 0);
final kBinaryNumberMode = Mode(className: "number", begin: "\\b(0b[01]+)", relevance: 0);
final kCssNumberMode = Mode(className: "number", begin: "\\b\\d+(\\.\\d+)?(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?", relevance: 0);
final kRegexpMode = Mode(className: "regexp", begin: "\\/", end: "\\/[gimuy]*", illegal: "\\n", contains: [
  Mode(begin: "\\\\[\\s\\S]", relevance: 0),
  Mode(begin: "\\[", end: "\\]", relevance: 0, contains: [Mode(begin: "\\\\[\\s\\S]", relevance: 0)]),
]);
final kTitleMode = Mode(className: "title", begin: "[a-zA-Z]\\w*", relevance: 0);
final kUnderscoreTitleMode = Mode(className: "title", begin: "[a-zA-Z_]\\w*", relevance: 0);
final kMethodGuard = Mode(begin: "\\.\\s*[a-zA-Z_]\\w*", relevance: 0);
