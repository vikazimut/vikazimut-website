import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';

import 'highlighter.dart' show highlight, Node;

class HighlightView extends StatelessWidget {
  final String source;
  final String language;
  final Map<String, TextStyle> theme;
  final EdgeInsetsGeometry? padding;
  final TextStyle? textStyle;

  HighlightView(
    String input, {
    required this.language,
    this.theme = const {},
    this.padding,
    this.textStyle,
    int tabSize = 8,
  }) : source = input.replaceAll('\t', ' ' * tabSize);

  List<TextSpan> _convert(List<Node> nodes) {
    List<TextSpan> spans = [];
    var currentSpans = spans;
    List<List<TextSpan>> stack = [];

    traverse(Node node) {
      if (node.value != null) {
        currentSpans.add(node.className == null ? TextSpan(text: node.value) : TextSpan(text: node.value, style: theme[node.className!]));
      } else if (node.children != null) {
        List<TextSpan> tmp = [];
        currentSpans.add(TextSpan(children: tmp, style: theme[node.className!]));
        stack.add(currentSpans);
        currentSpans = tmp;
        for (var n in node.children!) {
          traverse(n);
          if (n == node.children!.last) {
            currentSpans = stack.isEmpty ? spans : stack.removeLast();
          }
        }
      }
    }

    for (var node in nodes) {
      traverse(node);
    }

    return spans;
  }

  static const _rootKey = 'root';
  static const _defaultFontColor = Color(0xff000000);
  static const _defaultBackgroundColor = Color(0xffffffff);
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var textStyle = TextStyle(
      fontFamily: 'monospace',
      color: theme[_rootKey]?.color ?? _defaultFontColor,
    );
    textStyle = textStyle.merge(textStyle);
    return Scrollbar(
      thumbVisibility: true,
      controller: _scrollController,
      child: SingleChildScrollView(
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        child: Container(
          color: theme[_rootKey]?.backgroundColor ?? _defaultBackgroundColor,
          padding: padding,
          margin: const EdgeInsets.only(bottom: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: const BoxConstraints(maxWidth: 40),
                padding: const EdgeInsets.only(
                  right: 5,
                ),
                decoration: const BoxDecoration(
                  border: Border(
                    right: BorderSide(color: Colors.grey, width: 1),
                  ),
                ),
                child: Column(
                  children: _generateLineNumber(source),
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              SelectableText.rich(
                TextSpan(
                  style: textStyle,
                  children: _convert(highlight.parse(source, language).nodes!),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _generateLineNumber(String source) {
    var numberOfLine = source.split("\n").length;
    List<Widget> result = [];
    for (int i = 0; i < numberOfLine; i++) {
      result.add(Text(
        sprintf("%4d", [i + 1]),
        style: const TextStyle(color: Colors.grey),
      ));
    }
    return result;
  }
}
