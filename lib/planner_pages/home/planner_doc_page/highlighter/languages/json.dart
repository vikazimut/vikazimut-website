// GENERATED CODE - DO NOT MODIFY BY HAND

import '../mode.dart';
import '../common_modes.dart';

final json = Mode(refs: {
  '~contains~2~contains~1~contains~3': Mode(
      begin: "\\[",
      end: "\\]",
      contains: [
        Mode(end: ",", endsWithParent: true, excludeEnd: true, contains: [
          kQuoteStringMode,
          kCNumberMode,
          Mode(ref: '~contains~2'),
          Mode(ref: '~contains~2~contains~1~contains~3'),
          kCLineCommentMode,
          kCBlockCommentMode
        ], keywords: {
          "literal": "true false null"
        })
      ],
      illegal: "\\S"),
  '~contains~2': Mode(
      begin: "{",
      end: "}",
      contains: [
        Mode(
            className: "attr",
            begin: "\"",
            end: "\"",
            contains: [kBackslashEscape],
            illegal: "\\n"),
        Mode(
            end: ",",
            endsWithParent: true,
            excludeEnd: true,
            contains: [
              kQuoteStringMode,
              kCNumberMode,
              Mode(ref: '~contains~2'),
              Mode(ref: '~contains~2~contains~1~contains~3'),
              kCLineCommentMode,
              kCBlockCommentMode
            ],
            keywords: {"literal": "true false null"},
            begin: ":"),
        kCLineCommentMode,
        kCBlockCommentMode
      ],
      illegal: "\\S"),
}, contains: [
  kQuoteStringMode,
  kCNumberMode,
  Mode(ref: '~contains~2'),
  Mode(ref: '~contains~2~contains~1~contains~3'),
  kCLineCommentMode,
  kCBlockCommentMode
], keywords: {
  "literal": "true false null"
}, illegal: "\\S");
