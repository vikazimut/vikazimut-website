# Contributing to the development of the Vikazimut website

This project is the frontend part of the website.
It is developed in Dart/Flutter.
The web backend part is developed in Symfony and is located at
[https://gitlab.com/vikazimut/vikazimut-web](https://gitlab.com/vikazimut/vikazimut-web).
Communication between the backend and the frontend uses the REST API.

## License

This project is licensed under the terms of the GNU Lesser General Public
License [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html),
which requires that future developments are also made under this license.

## 1. Install Flutter

See the Flutter official documentation.

## 2. Install distribution

The source code is available on Gitlab
repository [https://gitlab.com/clouardregis/vikazimut-website](https://gitlab.com/clouardregis/vikazimut-website).

However, the file `/lib/keys.dart` that contains the orientation map image encryption keys is
missing. This file must be requested from the project manager.

## 3. Configure Chrome to avoid CORS issues

If you use Chrome as the local server during development (default behavior),
you should configure Flutter to avoid CORS issues.

Assume Flutter is installed in folder `~/snap/flutter`:

1. Go to `~/snap/flutter/common/flutter/bin/cache` and remove the file named `flutter_tools.stamp`

2. Go to `~/snap/flutter/common/flutter/packages/flutter_tools/lib/src/web` and open the
   file `chrome.dart`.

3. Find `'--disable-extensions'`

4. Add above `'--disable-web-security'`

After launching Chrome, a security warning message should appear at the top of the Chrome browser,
which prove the modification is taken into account.

## 4. Deploy the website on the Vik'Azim domain

The website is hosted at [https://vikazimut.vikazim.fr](https://vikazimut.vikazim.fr).
Authentication information must be requested from the project manager.

To deploy a release on Linux, open a terminal at the root of the project and type in:

```
$ flutter build web --release --base-href=/web/
$ ./deploy.sh
```

The command `deploy.sh` copies all the files (in fact only differences) from the
directory `build/web` to the web hosting using the Unix command `scp`.

The file `deploy.sh` contains the command `rsync`:

```
$ rsync -av ./build/web  u51382475@home468677337.1and1-data.host:~/appli-vikazimut --exclude=".*" --exclude "*~" --exclude "deploy.sh"
```

### Note

Docker cannot be used since there is not way to install docker
with our current web hosting account.

