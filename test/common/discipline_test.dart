import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/discipline.dart';

void main() {
  test('Test isFooto when null', () {
    var actual = Discipline.isFootOrienteering(null);
    expect(actual, true);
  });

  test('Test isFooto when URBANO', () {
    var actual = Discipline.isFootOrienteering(Discipline.URBANO);
    expect(actual, true);
  });

  test('Test isFooto when FORESTO', () {
    var actual = Discipline.isFootOrienteering(Discipline.FORESTO);
    expect(actual, true);
  });

  test('Test isFooto when MTBO', () {
    var actual = Discipline.isFootOrienteering(Discipline.MTBO);
    expect(actual, false);
  });

  test('Test isMTOB when null', () {
    var actual = Discipline.isMTBOrienteering(null);
    expect(actual, false);
  });

  test('Test isMTOB when UrbanO', () {
    var actual = Discipline.isMTBOrienteering(Discipline.URBANO);
    expect(actual, false);
  });

  test('Test isMTOB when MTBO', () {
    var actual = Discipline.isMTBOrienteering(Discipline.MTBO);
    expect(actual, true);
  });
}
