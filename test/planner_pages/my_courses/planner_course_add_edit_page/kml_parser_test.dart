import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/kml_data.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/kml_parser.dart';
import 'package:xml/xml.dart';

void main() {
  test('Test check kml format nominal case', () {
    String xmlHeader = """<?xml version="1.0" encoding="UTF-8" ?>
<!-- Generator: OCAD Version 12.4.0 -->
<kml xmlns="http://www.opengis.net/kml/2.2">
</kml>
""";
    var result = KmlParser.checkFormat(xmlHeader);
    expect(result, true);
  });

  test('Test check kml format error case', () {
    String xmlHeader = """<?xml version="1.0" encoding="UTF-8" ?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2020-07-13T16:26:16.120+02:00" creator="OCAD 12.4.0.1684">
</CourseData>
""";
    var result = KmlParser.checkFormat(xmlHeader);
    expect(result, false);
  });

  test('Test check kml format when true', () {
    File xmlFile = File('test/resources/38/38.kml');
    String xmlAsString = xmlFile.readAsStringSync();
    final XmlDocument document = XmlDocument.parse(xmlAsString);
    expect(true, KmlParser.checkKmlHeader_(document));
  });

  test('Test check kml format when false', () {
    File xmlFile = File('test/resources/38/38.xml');
    String xmlAsString = xmlFile.readAsStringSync();
    final XmlDocument document = XmlDocument.parse(xmlAsString);
    expect(false, KmlParser.checkKmlHeader_(document));
  });

  test('Test extract LatLonBox from kml', () {
    const String kml = """
   <groundoverlay>
		<name>tile_0_0.jpg</name>
		<drawOrder>75</drawOrder>
		<icon>
			<href>files/tile_0_0.jpg</href>
			<viewBoundScale>0.75</viewBoundScale>
		</icon>
		<latlonbox>
			<north>49.233194700</north>
			<south>49.222787250</south>
			<east>-0.293598224</east>
			<west>-0.304783960</west>
			<rotation>2.397735503</rotation>
		</latlonbox>
	</groundoverlay>
	""";
    final XmlDocument document = XmlDocument.parse(kml);
    XmlElement? groundOverlay = document.getElement("groundoverlay");
    KmlData expected = KmlData(georeferencing: const LatLonBox(north: 49.233194700, south: 49.222787250, east: -0.293598224, west: -0.304783960, rotation: 2.397735503), imageFilename: "files/tile_0_0.jpg", courseName: "");
    var actual = KmlParser.extractLatLonBoxAndImageName_(groundOverlay!);
    expect(actual.georeferencing.north, expected.georeferencing.north);
    expect(actual.georeferencing.south, expected.georeferencing.south);
    expect(actual.georeferencing.east, expected.georeferencing.east);
    expect(actual.georeferencing.west, expected.georeferencing.west);
    expect(actual.georeferencing.rotation, expected.georeferencing.rotation);
    expect(actual.imageFilename, expected.imageFilename);
  });

  test('Test extract LatLonQuad from kml for one course', () {
    const String kml = """
    <groundoverlay id="PNG" >
      <name>PNG</name>
      <icon>
        <href>files/V_X_L.png</href>
      </icon>
      <gx:latlonquad>
        <coordinates>
            -1.2038766706,44.9584112415,0 -1.1772470337,44.9582926744,0 -1.1770043906,44.9850330302,0 -1.2036467344,44.9851516548,0
        </coordinates>
      </gx:latlonquad>
    </groundoverlay>
    """;
    final XmlDocument document = XmlDocument.parse(kml);
    XmlElement? groundOverlay = document.getElement("groundoverlay");
    LatLonBox expected = const LatLonBox(
      north: 44.985092342499996,
      south: 44.95835195795,
      east: -1.17712571215,
      west: -1.2037617025,
      rotation: -0.36059243608478453,
    );
    var actual = KmlParser.extractLatLonQuadAndImage_(groundOverlay!);
    expect(actual.georeferencing.north, expected.north);
    expect(actual.georeferencing.south, expected.south);
    expect(actual.georeferencing.east, expected.east);
    expect(actual.georeferencing.west, expected.west);
    expect(actual.georeferencing.rotation, expected.rotation);
  });

  test('Test read KML file with one course', () {
    File xmlFile = File('test/resources/38/38.kml');
    String xmlAsString = xmlFile.readAsStringSync();
    final (maps, mapImageData) = KmlParser.getAllGeoreferencedMapsFromKml(xmlAsString);
    KmlData expected = KmlData(
      georeferencing: const LatLonBox(north: 49.2331947, east: -0.293598224, south: 49.22278725, west: -0.304783960, rotation: 2.397735503),
      imageFilename: "files/tile_0_0.jpg",
      courseName: null,
    );
    expect(maps[0].georeferencing.north, expected.georeferencing.north);
    expect(maps[0].georeferencing.south, expected.georeferencing.south);
    expect(maps[0].georeferencing.east, expected.georeferencing.east);
    expect(maps[0].georeferencing.west, expected.georeferencing.west);
    expect(maps[0].imageFilename, expected.imageFilename);
    expect(maps[0].courseName, expected.courseName);
  });

  test('Test read KML file with multiple courses', () {
    File xmlFile = File('test/resources/lacanau/Doc.kml');
    String xmlAsString = xmlFile.readAsStringSync();
    List<KmlData> expected = [
      KmlData(
        georeferencing: const LatLonBox(
          north: 44.985092342499996,
          east: -1.17712571215,
          south: 44.95835195795,
          west: -1.2037617025,
          rotation: -0.36059243608478453,
        ),
        imageFilename: "V_X_L.png",
        courseName: "V_X_L",
      ),
      KmlData(
        georeferencing: const LatLonBox(
          north: 44.985092342499996,
          east: -1.17712571215,
          south: 44.95835195795,
          west: -1.2037617025,
          rotation: -0.36059243608478453,
        ),
        imageFilename: "Bleu.png",
        courseName: "Bleu",
      ),
      KmlData(
        georeferencing: const LatLonBox(
          north: 44.985092342499996,
          east: -1.17712571215,
          south: 44.95835195795,
          west: -1.2037617025,
          rotation: -0.36059243608478453,
        ),
        imageFilename: "Jaune.png",
        courseName: "Jaune",
      ),
      KmlData(
        georeferencing: const LatLonBox(
          north: 44.985092342499996,
          east: -1.17712571215,
          south: 44.95835195795,
          west: -1.2037617025,
          rotation: -0.36059243608478453,
        ),
        imageFilename: "V'C'.png",
        courseName: "V'C'",
      ),
      KmlData(
        georeferencing: const LatLonBox(
          north: 44.985092342499996,
          east: -1.17712571215,
          south: 44.95835195795,
          west: -1.2037617025,
          rotation: -0.36059243608478453,
        ),
        imageFilename: "V M_&'.png",
        courseName: "V M_&'",
      ),
    ];
    final (maps, mapImageData) = KmlParser.getAllGeoreferencedMapsFromKml(xmlAsString , []);
    expect(maps.length, 5);
    for (var i = 0; i < maps.length; ++i) {
      expect(maps[i].georeferencing.north, expected[i].georeferencing.north);
      expect(maps[i].georeferencing.south, expected[i].georeferencing.south);
      expect(maps[i].georeferencing.east, expected[i].georeferencing.east);
      expect(maps[i].georeferencing.west, expected[i].georeferencing.west);
      expect(maps[i].georeferencing.rotation, moreOrLessEquals(expected[i].georeferencing.rotation, epsilon: 0.01));
      expect(maps[i].imageFilename, expected[i].imageFilename);
      expect(maps[i].courseName, expected[i].courseName);
    }
    expect(true, true);
  });

  test('Test convert LatLonQuad to LatLonBox with rotation case 1', () {
    List<double> coordinates = <double>[
      -1.1980727510,
      45.0069280800,
      -1.1730498690,
      45.0078699000,
      -1.1749490560,
      45.0333276400,
      -1.1999832830,
      45.0323853900,
    ];
    LatLonBox expected = const LatLonBox(
      west: -1.199028434,
      east: -1.173999880,
      north: 45.032857200,
      south: 45.007399670,
      rotation: 3.046154186,
    );

    LatLonBox actual = KmlParser.convertLatLonQuadToLatLonBox_(coordinates);
    const double epsilon = 0.000001;
    expect(actual.west, moreOrLessEquals(expected.west, epsilon: epsilon));
    expect(actual.north, moreOrLessEquals(expected.north, epsilon: epsilon));
    expect(actual.south, moreOrLessEquals(expected.south, epsilon: epsilon));
    expect(actual.east, moreOrLessEquals(expected.east, epsilon: epsilon));
    expect(actual.rotation, moreOrLessEquals(expected.rotation, epsilon: 0.01));
  });
}
