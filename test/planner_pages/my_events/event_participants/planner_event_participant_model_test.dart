import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_participants/planner_event_participant_model.dart';

void main() {
  test('test event detail get type name 0', () {
    var actual = PlannerEventParticipantModel.fromJson({"id": 1, "name": "nickname"});
    expect(actual.id, 1);
    expect(actual.name, "nickname");
  });
}
