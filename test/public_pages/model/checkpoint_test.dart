import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';

void main() {
  test('Test Checkpoint.fromJson', () {
    var json = {"latitude": 49.040639, "longitude": -0.446552, "id": "S7", "index": 0, "type": "Start"};
    var actual = Checkpoint.fromJson(json);
    expect(actual.latitude, 49.040639);
    expect(actual.longitude, -0.446552);
    expect(actual.index, 0);
    expect(actual.id, "S7");
    expect(actual.type, "Start");
  });

  test('test addCheckpoints', () {
    List<dynamic> json = [
      {
        "location": [49.040639, -0.446552],
        "id": "S7",
        "index": 0,
        "type": "Start",
        "score": 0
      },
      {
        "location": [49.041372, -0.444322],
        "id": "72",
        "index": 1,
        "type": "Control",
        "score": 1
      },
      {
        "location": [49.04078, -0.438739],
        "id": "55",
        "index": 2,
        "type": "Control",
        "score": 2
      },
      {
        "location": [49.04274, -0.438493],
        "id": "53",
        "index": 3,
        "type": "Control",
        "score": 3
      },
      {
        "location": [49.041521, -0.434776],
        "id": "50",
        "index": 4,
        "type": "Control",
        "score": 4
      },
      {
        "location": [49.04085, -0.433416],
        "id": "51",
        "index": 5,
        "type": "Control",
        "score": 5
      },
    ];

    List<Checkpoint> actual = Checkpoint.addCheckpoints_(json);
    expect(actual[0].latitude, 49.040639);
    expect(actual[0].type, "Start");
    expect(actual[1].longitude, -0.444322);
    expect(actual[2].id, "55");
    expect(actual[3].index, 3);
    expect(actual[0].score, 0);
    expect(actual[4].score, 4);
  });
}
