import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/courses/nearby_courses/nearby_course_model.dart';
import 'package:vikazimut_website/public_pages/courses/nearby_courses/nearby_courses_page_presenter.dart';

void main() {
  test('test buildCourseList', () {
    String jsonData = """
[{"id":1,"name":"c1","tracks":1,"distance":0,"club":"club"},
 {"id":2,"name":"c2","tracks":2,"distance":0,"club":""},
 {"id":3,"name":"c3","tracks":3,"distance":50,"club":"club","cluburl":"https://vikazim.fr","printable":true},
 {"id":4,"name":"c4","tracks":4,"distance":100},
 {"id":5,"name":"c5","tracks":5,"distance":2000}]
""";
    List<NearbyCourseModel> result = NearbyCoursesPagePresenter().buildCourseList_(jsonData, null);
    expect(result.length, 5);
    expect(result[0].id, 1);
    expect(result[1].id, 2);
    expect(result[2].club, "club");
    expect(result[2].clubUrl, "https://vikazim.fr");
    expect(result[3].distance, 100);
    expect(result[4].distance, 2000);
  });

  test('test compareNearbyCourses when equal distance but increasing name', () {
    NearbyCourseModel a = const NearbyCourseModel(id: 0, name: "name1", tracks: 0, distance: 0);
    NearbyCourseModel b = const NearbyCourseModel(id: 1, name: "name2", tracks: 0, distance: 0);
    int result = NearbyCoursesPagePresenter.compareNearbyCourses(a, b, null);
    expect(result, -1);
  });
  test('test compareNearbyCourses when equal distance but decreasing name', () {
    NearbyCourseModel a = const NearbyCourseModel(id: 0, name: "name2", tracks: 0, distance: 0);
    NearbyCourseModel b = const NearbyCourseModel(id: 1, name: "name1", tracks: 0, distance: 0);
    int result = NearbyCoursesPagePresenter.compareNearbyCourses(a, b, null);
    expect(result, 1);
  });

  test('test compareNearbyCourses when increasing distance', () {
    NearbyCourseModel a = const NearbyCourseModel(id: 0, name: "name2", tracks: 0, distance: 1);
    NearbyCourseModel b = const NearbyCourseModel(id: 1, name: "name1", tracks: 0, distance: 2);
    int result = NearbyCoursesPagePresenter.compareNearbyCourses(a, b, null);
    expect(result, -1);
  });

  test('test compareNearbyCourses when decreasing distance', () {
    NearbyCourseModel a = const NearbyCourseModel(id: 0, name: "name2", tracks: 0, distance: 2);
    NearbyCourseModel b = const NearbyCourseModel(id: 1, name: "name1", tracks: 0, distance: 3);
    int result = NearbyCoursesPagePresenter.compareNearbyCourses(a, b, null);
    expect(result, -1);
  });

  test('test compareNearbyCourses when min distance and with id', () {
    NearbyCourseModel a = const NearbyCourseModel(id: 1, name: "name1", tracks: 0, distance: 10);
    NearbyCourseModel b = const NearbyCourseModel(id: 2, name: "name2", tracks: 0, distance: 10);
    int result = NearbyCoursesPagePresenter.compareNearbyCourses(a, b, 2);
    expect(result, 1);
  });
}
