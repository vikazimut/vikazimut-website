import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/global_grade_widget.dart';

void main() {
  test('Test get color from grade when 0', () {
    var actual = GlobalGradeWidget.getColorFromGrade_(0);
    expect(actual, GlobalGradeWidget.redColor);
  });
  test('Test get color form grade when 10', () {
    var actual = GlobalGradeWidget.getColorFromGrade_(10);
    expect(actual, GlobalGradeWidget.greenColor);
  });

  test('Test get color form grade when 5', () {
    var actual = GlobalGradeWidget.getColorFromGrade_(5);
    expect(actual, GlobalGradeWidget.orangeColor);
  });

  test('Test get color form grade when 7', () {
    var actual = GlobalGradeWidget.getColorFromGrade_(7);
    expect(actual, GlobalGradeWidget.yellowColor);
  });
}
