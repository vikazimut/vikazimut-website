import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/keypoint_indicator_processor.dart';

void main() {
  test('Test removeValidationMomentFromRoute case 1', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromDistance_(points, 16);
    expect(actual[0].timestampInMillisecond, 1032);
    expect(actual[0].latitude, 0.0001016);
    expect(actual[0].longitude, 0.0001016);
    expect(actual[0].altitude, 10.32);
    expect(actual[1].timestampInMillisecond, 3000);
  });

  test('Test removeValidationMomentFromRoute case 2', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromDistance_(points, 32);
    expect(actual[0].timestampInMillisecond, 3032);
    expect(actual[0].latitude, 0.0002032);
    expect(actual[0].longitude, 0.0002032);
    expect(actual[0].altitude, 30.32);
    expect(actual[1].timestampInMillisecond, 4000);
  });

  test('Test removeValidationMomentFromRoute case 3', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromDistance_(points, 100);
    expect(actual[0].latitude, 0);
    expect(actual[0].longitude, 0);
    expect(actual[0].timestampInMillisecond, 0);
  });

  test('Test skipFirst2Seconds cas nominal 1', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 3000, 30),
      GeodesicPoint(3, 3, 4000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual[0].latitude, 1.5);
    expect(actual[0].longitude, 1.5);
    expect(actual[0].timestampInMillisecond, 2000);
    expect(actual[0].altitude, 20);
  });

  test('Test skipFirst2Seconds cas nominal 2', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 5000, 30),
      GeodesicPoint(3, 3, 6000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual[0].latitude, 1.25);
    expect(actual[0].longitude, 1.25);
    expect(actual[0].timestampInMillisecond, 2000);
    expect(actual[0].altitude, 15);
  });

  test('Test skipFirst2Seconds cas nominal 3', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 2000, 30),
      GeodesicPoint(3, 3, 6000, 40),
    ];
    final actual = ControlExitKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual[0].latitude, 2);
    expect(actual[0].longitude, 2);
    expect(actual[0].timestampInMillisecond, 2000);
    expect(actual[1].timestampInMillisecond, 6000);
    expect(actual[0].altitude, 30);
  });
}
