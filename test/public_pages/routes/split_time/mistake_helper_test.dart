import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/mistake_helper.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/time_cell.dart';

void main() async {
  test('Test isGreaterThanOneMinuteForAllFourPunchTimes false', () {
    List<List<TimeCell>> timeCells = [
      [TimeCell()],
      [TimeCell()..setTimes(10, 10)..setTimesRank(2, 3)],
      [TimeCell()..setTimes(6, 6)..setTimesRank(1, 1)],
      [TimeCell()..setTimes(11, 11)..setTimesRank(3, 2)],
      [TimeCell()..setTimes(12, 12)..setTimesRank(4, 4)],
    ];
    var actual = MistakeHelper.bestTime(timeCells, 0);
    expect(actual, 6);
  });
}
