import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/time/time_sheet_model.dart';

void main() async {
  test('Test computeOvercostPercentage case 0', () {
    double actualDistance = 0;
    double theoreticalDistance = 0;
    double actual = TimeSheetModel.computeOvercostPercentage(actualDistance, theoreticalDistance);
    expect(actual, 0);
  });

  test('Test computeOvercostPercentage case nominal', () {
    double actualDistance = 110;
    double theoreticalDistance = 100;
    double actual = TimeSheetModel.computeOvercostPercentage(actualDistance, theoreticalDistance);
    expect(actual, 10);
  });
}
