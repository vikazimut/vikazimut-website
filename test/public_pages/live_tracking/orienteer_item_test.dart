import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/live_tracking/orienteer_item.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

void main() {
  test('Test old fromJson', () {
    var jsonContents = {
      "latitudes": "49.279202",
      "longitudes": "-0.425130",
      "id": 1,
      "nickname": "Toto",
      "startTimestamp": 1,
    };
    OrienteerItem orienteer = OrienteerItem.fromJson(jsonContents);
    expect(orienteer.currentPositions[0].latitude, double.parse(jsonContents["latitudes"] as String));
    expect(orienteer.currentPositions[0].longitude, double.parse(jsonContents["longitudes"] as String));
    expect(orienteer.id, jsonContents["id"]);
    expect(orienteer.nickname, jsonContents["nickname"]);
    expect(orienteer.startDateInMilliseconds, (jsonContents["startTimestamp"] as int) * 1000);
    expect(orienteer.score, null);
  });

  test('Test fromJson', () {
    var jsonContents = {
      "latitudes": "49.279202|50.279202",
      "longitudes": "-0.425130|-1.425130",
      "id": 1,
      "nickname": "Toto",
      "startTimestamp": 1,
      "score": 2,
    };
    OrienteerItem orienteer = OrienteerItem.fromJson(jsonContents);
    expect(orienteer.currentPositions[0].latitude, 49.279202);
    expect(orienteer.currentPositions[1].latitude, 50.279202);
    expect(orienteer.currentPositions[0].longitude, -0.425130);
    expect(orienteer.currentPositions[1].longitude, -1.425130);
    expect(orienteer.id, jsonContents["id"]);
    expect(2, jsonContents["score"]);
    expect(orienteer.nickname, jsonContents["nickname"]);
    expect(orienteer.startDateInMilliseconds, (jsonContents["startTimestamp"] as int) * 1000);
  });

  test('Test parsePositions with 1 position', () {
    List<GeodesicPoint> positions = OrienteerItem.parsePositions("49.279202", "-0.425130");
    expect(positions.length, 1);
    expect(positions[0].latitude, 49.279202);
    expect(positions[0].longitude, -0.425130);
  });

  test('Test parsePositions with 2 positions', () {
    List<GeodesicPoint> positions = OrienteerItem.parsePositions("49.279202|50.279202", "-0.425130|-1.425130");
    expect(positions.length, 2);
    expect(positions[0].latitude, 49.279202);
    expect(positions[1].latitude, 50.279202);
    expect(positions[0].longitude, -0.425130);
    expect(positions[1].longitude, -1.425130);
  });

  test('Test update tail', () {
    final positions = <GeodesicPoint>[
      GeodesicPoint(0, 0),
      GeodesicPoint(1, 1),
      GeodesicPoint(2, 2),
      GeodesicPoint(3, 3),
    ];
    final orienteer = OrienteerItem(
      id: 0,
      nickname: "toto",
      score: 0,
      startDateInMilliseconds: 0,
      currentPositions: List.from(positions),
    );
    expect(orienteer.currentPosition, positions[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[2]);
    expect(orienteer.tailPositions.length, 1);
    expect(orienteer.tailPositions[0], positions[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[1]);
    expect(orienteer.tailPositions.length, 2);
    expect(orienteer.tailPositions[0], positions[2]);
    expect(orienteer.tailPositions[1], positions[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[0]);
    expect(orienteer.tailPositions.length, 3);
    expect(orienteer.tailPositions[0], positions[1]);
    expect(orienteer.tailPositions[1], positions[2]);
    expect(orienteer.tailPositions[2], positions[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[0]);
    expect(orienteer.tailPositions.length, 4);
    expect(orienteer.tailPositions[0], positions[0]);
    expect(orienteer.tailPositions[1], positions[1]);
    expect(orienteer.tailPositions[2], positions[2]);
    expect(orienteer.tailPositions[3], positions[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[0]);
    expect(orienteer.tailPositions.length, 4);
    expect(orienteer.tailPositions[0], positions[0]);
    expect(orienteer.tailPositions[1], positions[0]);
    expect(orienteer.tailPositions[2], positions[1]);
    expect(orienteer.tailPositions[3], positions[2]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions[0]);
    expect(orienteer.tailPositions.length, 4);
    expect(orienteer.tailPositions[0], positions[0]);
    expect(orienteer.tailPositions[1], positions[0]);
    expect(orienteer.tailPositions[2], positions[0]);
    expect(orienteer.tailPositions[3], positions[1]);
    final positions2 = <GeodesicPoint>[
      GeodesicPoint(10, 10),
      GeodesicPoint(11, 11),
      GeodesicPoint(12, 12),
      GeodesicPoint(13, 13),
    ];
    orienteer.currentPositions = List.from(positions2);
    expect(orienteer.currentPosition, positions2[3]);
    orienteer.updateTail();
    expect(orienteer.currentPosition, positions2[2]);
    expect(orienteer.tailPositions.length, 4);
    expect(orienteer.tailPositions[0], positions2[3]);
    expect(orienteer.tailPositions[1].latitude, positions[0].latitude);
    expect(orienteer.tailPositions[2], positions[0]);
    expect(orienteer.tailPositions[3], positions[0]);
  });

  test('Test is not at same position when true', () {
    List<GeodesicPoint> positions = [
      GeodesicPoint(0, 0),
      GeodesicPoint(1, 1),
      GeodesicPoint(2, 2),
      GeodesicPoint(3, 3),
    ];
    var orienteerItem1 = OrienteerItem(
      currentPositions: List.from(positions),
      id: 1,
      nickname: "toto",
      startDateInMilliseconds: 0,
    );
    var orienteerItem2 = OrienteerItem(
      currentPositions: List.from(positions),
      id: 1,
      nickname: "toto",
      startDateInMilliseconds: 0,
    );
    expect(orienteerItem1.isNotSameLocations(orienteerItem2), false);
  });

  test('Test is not at same position when false', () {
    List<GeodesicPoint> positions1 = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(1, 1, 1),
      GeodesicPoint(2, 2, 2),
      GeodesicPoint(3, 3, 3),
    ];
    var orienteerItem1 = OrienteerItem(
      currentPositions: positions1,
      id: 1,
      nickname: "toto",
      startDateInMilliseconds: 0,
    );

    List<GeodesicPoint> positions2 = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(1, 1, 1),
      GeodesicPoint(2, 2, 2),
      GeodesicPoint(3, 4, 3),
    ];
    var orienteerItem2 = OrienteerItem(
      currentPositions: positions2,
      id: 1,
      nickname: "toto",
      startDateInMilliseconds: 0,
    );
    expect(orienteerItem1.isNotSameLocations(orienteerItem2), true);
  });
}
